package watcher

import (
	"sync"

	"vision/config"
	"vision/infra"

	"vision/common"

	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

var (
	RegionWatcherMap = map[string]*RegionWatcher{}
	RegionWatchMap   = map[string]bool{}
)

type RegionWatcher struct {
	Stop chan struct{}
	*infra.KubeClient
	StartTime        int64
	watchedResources map[string]*ResourceWatcher
	watchLock        sync.RWMutex
}

func shouldExpire(startTime int64) bool {
	return (common.GetCurrentTimeStamp() - startTime) >= int64(config.GlobalConfig.Watcher.Timeout)
}

const (
	InactiveTime = 60
)

type KubeWatcher struct {
	Redis *infra.RedisClient
	// Self is a id for this watcher
	Self string
}

func NewKubeWatcher() (*KubeWatcher, error) {
	redis, err := infra.GetRedis()
	if err != nil {
		log.WithError(err).Error("Get redis error")
		return nil, err
	}
	return &KubeWatcher{
		Redis: redis,
		Self:  common.NewUUID(),
	}, nil
}

func (w *KubeWatcher) RegisterSelf() error {
	r := w.Redis
	keys, err := r.HKeys(common.WatcherInstancesCacheKey)
	if err != nil {
		log.WithError(err).Error("Get watcher instances cache error when register self!")
		return err
	}
	log.Infof("Found exist watchers: %+v", keys)
	log.Info("Preparing to register self.")

	err = r.HSet(common.WatcherInstancesCacheKey, w.Self, cast.ToString(common.GetCurrentTimeStamp()))
	if err != nil {
		log.WithError(err).Error("Error register watcher instance")
		return err
	} else {
		log.Infof("Register self as %s", w.Self)
	}
	return nil
}

func (w *KubeWatcher) CleanUp() error {
	log.Info("And my watcher ended...")
	err := w.DeleteSelf()
	if err != nil {
		log.WithError(err).Error("Un-register self error")
	}

	flag, err := w.HasOthers()
	if err != nil {
		log.WithError(err).Error("error check others...")
		return err
	}

	if !flag {
		log.Info("No others found, delete caches.")
		err := w.DeleteWatcherKeys()
		if err != nil {
			log.WithError(err).Error("error delete kubernetes cache.")
			return err
		}

	}

	return nil
}

func (w *KubeWatcher) DeleteWatcherKeys() error {
	redis := w.Redis
	keys, err := infra.GetMatchCacheKeys("watcher:*")
	if err != nil {
		return err
	}
	if len(keys) == 0 {
		log.Info("No cache keys to delete")
		return nil
	} else {
		log.Infof("Found %d keys", len(keys))
	}
	count, err := redis.NoPrefixDel(keys...)
	if err != nil {
		return err
	}
	log.Infof("Deleted %d keys ", count)
	return nil

}

func (w *KubeWatcher) DeleteSelf() error {
	r := w.Redis
	_, err := r.HDel(common.WatcherInstancesCacheKey, w.Self)
	return err
}

func (w *KubeWatcher) Refresh() error {
	return w.Redis.HSet(common.WatcherInstancesCacheKey, w.Self, cast.ToString(common.GetCurrentTimeStamp()))
}

func (w *KubeWatcher) RemoveDead() error {
	keys, err := w.Redis.HKeys(common.WatcherInstancesCacheKey)
	if err != nil {
		return err
	}
	for _, key := range keys {
		if key == w.Self {
			continue
		}
		value, err := w.Redis.HGet(common.WatcherInstancesCacheKey, key)
		if err != nil {
			return err
		}

		if (common.GetCurrentTimeStamp() - cast.ToInt64(value)) >= int64(InactiveTime) {
			_, err := w.Redis.HDel(common.WatcherInstancesCacheKey, key)
			if err != nil {
				return err
			} else {
				log.Infof("Remove inactive instances: %s", key)
			}
		}

	}
	return nil
}

func (w *KubeWatcher) HasOthers() (bool, error) {
	keys, err := w.Redis.HKeys(common.WatcherInstancesCacheKey)
	if err != nil {
		return false, err
	}
	for _, key := range keys {
		if key != w.Self {
			return true, nil
		}
	}
	return false, nil
}

func setRegionWatcher(cid string) error {
	if RegionWatcherMap[cid] != nil {
		return nil
	}
	client, err := infra.NewKubeClient(cid)
	if err != nil {
		log.WithError(err).Error("Get kubernetes client error!")
		return err
	}
	RegionWatcherMap[cid] = &RegionWatcher{
		KubeClient:       client,
		StartTime:        common.GetCurrentTimeStamp(),
		Stop:             make(chan struct{}),
		watchedResources: make(map[string]*ResourceWatcher),
	}
	return nil
}

// StopWatch stop watch a region. key is the region id
func (w *KubeWatcher) StopWatch(key string) {
	log.Infof("Region %s not exist in cache, trying to stop the watch.", key)
	delete(RegionWatchMap, key)
	_, ok := RegionWatcherMap[key]
	if ok {
		close(RegionWatcherMap[key].Stop)
		RegionWatcherMap[key].StopAllWatchedResources()
		delete(RegionWatcherMap, key)
	}
	log.Info("Stop the watch for region:", key)

}

// WatchRegions will watch regions. It will periodically check keys in cache to find if there
// are new regions need to watch or regions to expire. Since a region can be clean up and re-deploy
// we need to check the access token of kubernetes to find out if this is a new kubernetes cluster
// under the same region.So we will have to periodically pull region info from furion too.
func (w *KubeWatcher) WatchRegions() error {
	log.Info("vision watcher started.")
	r := w.Redis

	for {

		if err := w.Refresh(); err != nil {
			log.WithError(err).Errorf("Refresh self error: %s", w.Self)
		}

		w.RemoveDead()

		time.Sleep(time.Second * 2)

		keys, err := r.HKeys(common.RegionWatcherKey)
		if err != nil {
			log.WithError(err).Error("Get region ids in cache error!")
		}

		for key := range RegionWatchMap {
			if !common.StringInSlice(key, keys) {
				w.StopWatch(key)
			}
		}

		for _, key := range keys {
			logger := common.GetLoggerByRegionID(key)
			logger.Debugf("Check region...")

			if !RegionWatchMap[key] {
				logger.Info("Start watch region.")
				RegionWatchMap[key] = true

				if err := setRegionWatcher(key); err != nil {
					delete(RegionWatchMap, key)
					logger.Error("Generate region client error, possibly incorrect region data.")
					continue
				} else {
					logger.Info("Set region watcher success.")
				}

				go func(watcherKey string) {
					// Watch resources until the watch is successful or the watcher is
					// stopped.
					watcher, ok := RegionWatcherMap[watcherKey]
					if !ok {
						// Watcher has been stopped.
						logger.Infof("Watcher %s has been stopped, stop for watching resource!", watcherKey)
						return
					}
					stop := watcher.Stop
					for {
						select {
						case <-stop:
							logger.Info("Watcher is stopped")
							watcher.StopAllWatchedResources()
							return
						default:
							err := watcher.WatchResources()
							if err == nil {
								return
							}
							logger.Error(err)
						}
						time.Sleep(time.Second * 1)
					}
				}(key)
				logger.Info("Watch for all selected resource done.")
			} else {
				newCluster, err := RegionWatcherMap[key].IsNewCluster()
				if err != nil {
					log.WithError(err).Error("Error retrieve region data")
					continue
				}

				if newCluster {
					logger.Info("A new cluster has been deployed, stop watch this now.")
					w.StopWatch(key)
					continue
				}

				value, err := r.HGet(common.RegionWatcherKey, key)
				if err != nil {
					log.WithError(err).Error("Retrieve region watch start time error")
					continue
				}
				startTime, err := cast.ToInt64E(value)
				if err != nil {
					startTime = 0
				}
				logger.Infof("region watcher : from: %s, current: %d, key: %s", value,
					common.GetCurrentTimeStamp(), key)
				RegionWatcherMap[key].StartTime = startTime
				if shouldExpire(startTime) {
					logger.Infof("Times up, trying to clean up this region: %s", key)
					_, err := r.HDel(common.RegionWatcherKey, key)
					if err != nil {
						log.WithError(err).Error("Delete expire region from watch cache error!")
						continue
					}
					log.WithField("region_id", key).Infof("Remove region from watch. start from %s",
						common.TimeStampToTime(startTime))
					w.StopWatch(key)
				}
			}
		}
	}
}
