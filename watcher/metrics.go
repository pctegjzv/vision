package watcher

import "github.com/prometheus/client_golang/prometheus"

var (
	EventCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "event_counter",
		Help: "number of events received since vision started",
	}, []string{"cluster", "namespace", "kind"})

	EventBytesCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "event_bytes_total_counter",
		Help: "bytes of all events received since vision started",
	}, []string{"cluster", "namespace", "kind"})

	EventAddCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "event_add_counter",
		Help: "number of add events received since vision started",
	}, []string{"cluster", "namespace", "kind"})

	EventDeleteCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "event_delete_counter",
		Help: "number of delete events received since vision started",
	}, []string{"cluster", "namespace", "kind"})

	EventUpdateCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "event_update_counter",
		Help: "number of update events received since vision started",
	}, []string{"cluster", "namespace", "kind"})
)
