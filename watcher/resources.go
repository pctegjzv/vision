package watcher

import (
	"vision/common"

	"k8s.io/apimachinery/pkg/runtime"
)

// KubernetesObj ...

// TODO: copy to common.

type KubernetesObj struct {
	apiVersion    string
	runtimeObject runtime.Object
}

var (
	HandlerKinds = map[string]bool{
		common.KubernetesKindNamespace: true,
	}

	// WatchBlackList defines a resource map that won't be watched.
	WatchBlackList = map[string]bool{
		// "pods":                true,
		"componentstatuses":   true,
		"controllerrevisions": true,
		"events":              true,
	}

	// TODO(xxhe): Used to cache by uuid for v3 resources, remove this
	// if v3 API have migrated to use name as key.
	v3Resources = map[string]bool{
		"services":               true,
		"namespaces":             true,
		"configmaps":             true,
		"persistentvolumes":      true,
		"persistentvolumeclaims": true,
		"clusterservicebrokers":  true,
		"clusterserviceclasses":  true,
		"clusterserviceplans":    true,
		"serviceinstances":       true,
		"servicebindings":        true,
	}
)
