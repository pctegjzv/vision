package watcher

import (
	"fmt"
	"strings"
	"time"

	"vision/common"
	"vision/config"
	"vision/infra"
	"vision/model"

	"github.com/json-iterator/go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cast"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	utilRuntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/tools/cache"
)

// IsNewCluster will check if the region has been re-deployed(a new kubernetes cluster)
func (w *RegionWatcher) IsNewCluster() (bool, error) {
	config, err := infra.GetKubernetesConfig(w.RegionID)
	if err != nil {
		return false, err
	}
	return config.Token != w.Token || config.Endpoint != w.Endpoint, nil
}

func (w *RegionWatcher) WatchResources() error {
	resources, err := w.ServerPreferredResources()
	if err != nil {
		return fmt.Errorf("get api resource list from cluster error: %v", err)
	}
	w.Logger.Debugf("Prepare resources to watch: %v", resources)

	for _, resourceList := range resources {
		gv, err := schema.ParseGroupVersion(resourceList.GroupVersion)
		if err != nil {
			w.Logger.Warnf("Parse group version '%s' error: %v", resourceList.GroupVersion, err)
			continue
		}
		for _, r := range resourceList.APIResources {
			if _, exist := WatchBlackList[r.Name]; exist {
				continue
			}
			rw := w.createResourceWatch(&r, gv)
			if rw != nil {
				w.watchResource(*rw)
			}
		}
	}
	return nil
}

func (w *RegionWatcher) createResourceWatch(r *v1.APIResource, gv schema.GroupVersion) *ResourceWatch {
	isSupportListWatchVerb := func(verbs v1.Verbs) bool {
		haveList := false
		haveWatch := false
		for _, v := range verbs {
			if strings.ToLower(v) == "list" {
				haveList = true
			} else if strings.ToLower(v) == "watch" {
				haveWatch = true
			}
			if haveList && haveWatch {
				return true
			}
		}
		return false
	}

	gvk := schema.GroupVersionKind{
		Group:   gv.Group,
		Version: gv.Version,
		Kind:    r.Kind,
	}

	// Only resources that supports 'list' and 'watch' verb
	// can be watched, and we use 'Unstructrured' type for
	// all watched resources.
	if isSupportListWatchVerb(r.Verbs) {
		return &ResourceWatch{
			Resource: r.Name,
			GVK:      gvk,
			ObjType:  &unstructured.Unstructured{},
		}
	}
	w.Logger.Warnf("Resource '%s' won't be watched for no support of list watch", r.Name)
	return nil
}

func (w *RegionWatcher) GetExpireTime() time.Duration {
	dur := w.StartTime + cast.ToInt64(config.GlobalConfig.Watcher.Timeout) - common.GetCurrentTimeStamp()
	return time.Second * time.Duration(dur)

}

// ResourceIsWatched return true if the specific resource is already watched.
func (w *RegionWatcher) ResourceIsWatched(resource string) (watched bool) {
	_, watched = w.watchedResources[resource]
	return
}

func (w *RegionWatcher) setResourceWatched(rw *ResourceWatcher) {
	w.watchedResources[rw.ResourceWatch.Resource] = rw
}

func (w *RegionWatcher) setResourceUnWatched(resource string) {
	delete(w.watchedResources, resource)
}

func (w *RegionWatcher) watchUnregisteredResource(resource string, gvk schema.GroupVersionKind) {
	w.Logger.Infof("Start to watch unregistered resource '%s'", resource)
	w.watchResource(ResourceWatch{
		resource, gvk, &unstructured.Unstructured{}})
}

// watchResource watch a single resource
func (w *RegionWatcher) watchResource(rw ResourceWatch) {
	w.watchLock.Lock()
	defer w.watchLock.Unlock()

	if w.ResourceIsWatched(rw.Resource) {
		return
	}

	rwatcher := NewResourceWatcher(w, rw)
	if rwatcher != nil {
		if err := rwatcher.Run(); err == nil {
			w.setResourceWatched(rwatcher)
			return
		}
	}
}

func (w *RegionWatcher) StopAllWatchedResources() {
	w.watchLock.Lock()
	defer w.watchLock.Unlock()

	for r, rw := range w.watchedResources {
		w.Logger.Infof("Stop watch resource '%s'", r)
		rw.Stop()
	}
	w.watchedResources = make(map[string]*ResourceWatcher)
}

func (w *RegionWatcher) StopWatchResource(resource string) {
	w.watchLock.Lock()
	defer w.watchLock.Unlock()

	if w.ResourceIsWatched(resource) {
		return
	}

	rwatcher, ok := w.watchedResources[resource]
	if ok {
		rwatcher.Stop()
		w.setResourceUnWatched(resource)
	}
}

func (w *RegionWatcher) getPromLabels(ks *model.KubernetesObject) prometheus.Labels {
	return prometheus.Labels{
		"cluster":   w.RegionID,
		"namespace": ks.Namespace,
		"kind":      ks.Kind,
	}
}

// deleteEvent delete resource's cache from redis.
func (w *RegionWatcher) deleteEvent(obj interface{}, rw ResourceWatch) {
	resource := rw.Resource
	gvk := rw.GVK
	var json = jsoniter.ConfigFastest
	deletedObj, ok := obj.(cache.DeletedFinalStateUnknown)
	if !ok {
		w.Logger.Errorf("Couldn't get deleted object for %s", rw.GVK)
		return
	}
	bytes, err := json.Marshal(deletedObj.Obj)
	if err != nil {
		w.Logger.Errorf("Marshal event object error: %s", err.Error())
		return
	}

	ks, _ := model.BytesToKubernetesObject(bytes)
	ks.Kind = gvk.Kind
	ks.APIVersion = gvk.GroupVersion().String()

	w.Logger.Debugf("delete object: %s %s/%s", ks.Kind, ks.Namespace, ks.Name)

	promLabels := w.getPromLabels(ks)
	EventDeleteCounter.With(promLabels).Inc()
	EventCounter.With(promLabels).Inc()
	EventBytesCounter.With(promLabels).Add(float64(len(bytes)))

	nameKey := infra.GetKubernetesNameCacheKey(w.RegionID, ks.Name, resource, ks.Namespace)

	if _, err = w.RedisClient.Del(nameKey); err != nil {
		w.Logger.WithError(err).Errorf("delete resource name key error: %s", nameKey)
	}

	w.DeleteHandler(ks)

	// TODO(xxhe): Delete cache by uuid for v3 resources, remove this if v3 API have
	// migrated to use name as key.
	if _, exist := v3Resources[resource]; exist {
		uid := string(ks.UID)
		uidKey := infra.GetKubernetesResourceCacheKey(uid)
		if _, err = w.RedisClient.Del(uidKey); err != nil {
			w.Logger.WithError(err).Errorf("del resource uid[%s] key cache error!", uidKey)
		} else {
			// w.Logger.Debugf("delete resource from cache: %s", uid)
		}
		// w.Logger.Info("Trying to delete jakiro resource for :", uid, nameKey)
		go infra.DeleteResourceFromJakiro(uid)
	}

}

// addEvent add object to cache and set two keys
// 1. object.uid -> object.unique_name
// 2. object.unique_name -> object.data
func (w *RegionWatcher) addEvent(obj interface{}, rw ResourceWatch, action string) {

	// Variables:
	var bytes []byte
	var err error

	defer utilRuntime.HandleCrash()

	resource := rw.Resource

	// Marshal obj into JSON:
	var json = jsoniter.ConfigFastest
	if bytes, err = json.Marshal(obj); err != nil {
		w.Logger.Error("Ops! Cannot marshal JSON")
		return
	}
	ks, err := model.BytesToKubernetesObject(bytes)
	if err != nil {
		w.Logger.WithError(err).Errorf("Convert %s to kubernetes resource error", resource)
		return
	}

	w.Logger.Debugf("%s object: %s %s/%s", action, ks.Kind, ks.Namespace, ks.Name)

	promLabels := w.getPromLabels(ks)
	EventCounter.With(promLabels).Inc()
	EventBytesCounter.With(promLabels).Add(float64(len(bytes)))

	nameKey := infra.GetKubernetesNameCacheKey(w.RegionID, ks.Name, resource, ks.Namespace)

	if err = w.RedisClient.Set(nameKey, bytes, w.GetExpireTime()); err != nil {
		w.Logger.WithError(err).Errorf("add resource data in cache error: %s", nameKey)
	} else {
		switch action {
		case EventAddAction:
			//TODO: remove toObject, interface{} to object
			EventAddCounter.With(promLabels).Inc()
			w.AddHandler(ks)
		case EventUpdateAction:
			EventUpdateCounter.With(promLabels).Inc()
			w.UpdateHandler(ks)
		}
	}

	// TODO(xxhe): Only cache by uuid for v3 resources, remove this if v3 API have
	// migrated to use name as key.
	if _, exist := v3Resources[resource]; exist {
		uid := model.GetAlaudaUID(ks)
		uidKey := infra.GetKubernetesResourceCacheKey(uid)
		if err = w.RedisClient.Set(uidKey, nameKey, w.GetExpireTime()); err != nil {
			w.Logger.WithError(err).Errorf("Set resource name key error: %s", nameKey)
		}
	}
}
