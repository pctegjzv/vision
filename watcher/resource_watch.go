package watcher

import (
	"fmt"

	"vision/common"

	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	utilRuntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/tools/cache"
)

type ResourceWatch struct {
	Resource string
	GVK      schema.GroupVersionKind
	ObjType  runtime.Object
}

type ResourceWatcher struct {
	ResourceWatch ResourceWatch
	watcher       *RegionWatcher
	logger        common.Log
	informer      cache.SharedInformer
	stop          chan struct{}
}

func NewResourceWatcher(watcher *RegionWatcher, rw ResourceWatch) *ResourceWatcher {
	logger := watcher.Logger.WithField("resource", rw.Resource)
	c, err := watcher.ClientForGroupVersionResource(rw.GVK.GroupVersion().String(), rw.Resource)
	if err != nil {
		logger.Errorf("Get the client for resource '%s' error: %v", rw.Resource, err)
		return nil
	}

	listWatch := cache.NewListWatchFromClient(c, rw.Resource, "", fields.Everything())
	informer := cache.NewSharedInformer(
		listWatch,
		rw.ObjType,
		0,
		//time.Second*time.Duration(config.GlobalConfig.Watcher.ResyncPeriod),
	)
	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			watcher.addEvent(obj, rw, EventAddAction)
		},
		UpdateFunc: func(old, new interface{}) {
			watcher.addEvent(new, rw, EventUpdateAction)
		},
		DeleteFunc: func(obj interface{}) {
			watcher.deleteEvent(obj, rw)
		},
	})

	return &ResourceWatcher{
		ResourceWatch: rw,
		watcher:       watcher,
		logger:        common.Log{Entry: logger},
		informer:      informer,
		stop:          make(chan struct{}),
	}
}

func (rw *ResourceWatcher) Run() error {
	defer utilRuntime.HandleCrash()

	rw.logger.Infof("Watching for new resource with kind '%s' type '%v'", rw.ResourceWatch.GVK, rw.ResourceWatch.ObjType)

	go rw.informer.Run(rw.stop)

	if !cache.WaitForCacheSync(rw.stop, rw.informer.HasSynced) {
		err := fmt.Errorf("timed out waiting for caches to sync")
		utilRuntime.HandleError(err)
		close(rw.stop)
		return err
	}
	rw.logger.Info("Resource Watcher informer synced and ready")
	return nil
}

func (rw *ResourceWatcher) Stop() {
	close(rw.stop)
	rw.logger.Info("Resource Watcher informer stopped")
}
