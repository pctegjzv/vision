package watcher

import (
	"encoding/json"
	"fmt"

	"vision/common"
	"vision/config"
	"vision/db"
	"vision/model"
	"vision/pkg/application/v1alpha1"
	kubeStore "vision/store"

	"vision/infra"

	"github.com/juju/errors"
	"github.com/spf13/cast"
	"github.com/thoas/go-funk"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/wait"
)

const (
	EventAddAction    = "add"
	EventUpdateAction = "update"
)

type ObjectEventHandler interface {
	CreateHandler(object *model.KubernetesObject, w *RegionWatcher) error
	UpdateHandler(object *model.KubernetesObject, w *RegionWatcher) error
	DeleteHandler(object *model.KubernetesObject, w *RegionWatcher) error
}

type BaseEventHandler struct {
}

func (n *BaseEventHandler) CreateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	return nil
}

func (n *BaseEventHandler) UpdateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	return nil
}

func (n *BaseEventHandler) DeleteHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	if object.Labels != nil && object.Kind != "Application" {
		// Handle with app resources.
		uuid, exist := object.Labels[common.AppUidKey()]
		if exist {
			return n.appResourcesDeleteHandler(uuid, object, w)
		}
	}
	return nil
}

func (n *BaseEventHandler) appResourcesDeleteHandler(appUUID string, object *model.KubernetesObject, w *RegionWatcher) error {
	logger := w.Logger.WithField("app", appUUID)

	locker, err := w.RedisClient.GetLocker(appUUID)
	if err != nil {
		return err
	}
	defer locker.Unlock()

	_, app, err := extractStatus(w, appUUID)
	if err != nil {
		return err
	}

	s := kubeStore.GetKubernetesResourceStore(&model.Query{
		ClusterUUID: app.Cluster,
		Name:        app.Crd.Name,
		Namespace:   app.Crd.Namespace,
		Type:        "applications",
	}, common.ToLog(logger), nil)

	application, err := s.GetApplication()
	if err != nil {
		logger.Errorf("Get application error: %s", err.Error())
		return UpdateApplicationPhase(app, v1alpha1.Failed)
	}

	appCrd := application.Crd.ToAppCrd()
	if appCrd.Spec.AssemblyPhase != v1alpha1.Pending {
		return nil
	}

	if len(application.Kubernetes) <= 0 {
		// All app resources deleted, delete the app now.
		if err := kubeStore.DeleteApplication(app, &app.Crd); err != nil {
			logger.Errorf("Delete app error: %s", err.Error())
			return UpdateApplicationPhase(app, v1alpha1.Failed)
		}
		logger.Infof("Delete app done")
	}
	return nil
}

type ApplicationEventHandler struct {
	BaseEventHandler
}

func GetHandler(kind string) ObjectEventHandler {
	switch kind {
	case "Application":
		return &ApplicationEventHandler{}
	case "Namespace":
		return &NamespaceEventHandler{}
	case "CustomResourceDefinition":
		return &CRDEventHandler{}
	default:
		return &BaseEventHandler{}
	}
}

func (w *RegionWatcher) AddHandler(object *model.KubernetesObject) {
	err := GetHandler(object.Kind).CreateHandler(object, w)
	if err != nil {
		w.Logger.WithError(err).Errorf("object create handler error: %s", object.GetString())
	}
}

func (w *RegionWatcher) UpdateHandler(object *model.KubernetesObject) {
	err := GetHandler(object.Kind).UpdateHandler(object, w)
	if err != nil {
		w.Logger.WithError(err).Error("object update handler error")
	}
}

func (w *RegionWatcher) DeleteHandler(object *model.KubernetesObject) {
	err := GetHandler(object.Kind).DeleteHandler(object, w)
	if err != nil {
		w.Logger.WithError(err).Error("object delete handler error")
	}
}

func UpdateApplicationPhase(application *model.Application, phase v1alpha1.ApplicationAssemblyPhase) error {
	return kubeStore.PatchApplication(application, model.GenSimpleApplicationUpdateData(string(phase), "", ""))
}

func (w *RegionWatcher) getResourceStatus(uuid string) (*model.AlaudaResourceStatus, error) {
	var status model.AlaudaResourceStatus

	cond := func() (done bool, err error) {
		w.Logger.Infof("Trying to get application related resource: %s", uuid)
		s, err := db.GetStatus(uuid)
		if err != nil {
			w.Logger.WithError(err).Warn("get application related resource error")
			return false, nil
		}
		w.Logger.Info("Get application resource from db")
		status = *s
		return true, nil
	}
	err := wait.Poll(common.GetTimeDuration(2), common.GetTimeDuration(6), cond)
	if err != nil {
		return nil, err
	}

	return &status, nil
}

func extractStatus(w *RegionWatcher, uuid string) (*model.AlaudaResourceStatus, *model.Application, error) {
	status, err := w.getResourceStatus(uuid)
	if err != nil {
		return nil, nil, err
	}
	app, err := status.ToApplication()
	if err != nil {
		return nil, nil, err
	}
	return status, app, err
}

func (a *ApplicationEventHandler) CreateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	uuid := object.Labels[common.AppUidKey()]
	logger := w.Logger.WithField("app", uuid)

	if object.GetAnnotations() != nil {
		if object.GetAnnotations()[common.AppMigrationKey()] != "" {
			logger.Info("This is a migration app, skip create handler")
			return nil
		}
	}

	locker, err := w.RedisClient.GetLocker(uuid)
	if err != nil {
		return err
	}
	defer locker.Unlock()

	_, app, err := extractStatus(w, uuid)
	if err != nil {
		return err
	}

	model.SortKubernetesObjects(app.Kubernetes, false)
	if err := kubeStore.CreateApplicationObjects(app, app.Kubernetes); err != nil {
		logger.Errorf("Create app sub-resource %v error: %v.", app.GetUUID(), err.Error())
		w.SendApplicationWarningEvent(object, err)
		return UpdateApplicationPhase(app, v1alpha1.Failed)
	}

	w.SendApplicationNormalEvent(object, "All sub resources have been synced", common.ReasonSynced)

	return UpdateApplicationPhase(app, v1alpha1.Succeeded)
}

// StartApplication will scale all workloads objects to it's origin replicas
func StartApplication(object *model.KubernetesObject, w *RegionWatcher) error {
	s := kubeStore.GetKubernetesResourceStore(&model.Query{
		ClusterUUID: w.RegionID,
		Namespace:   object.Namespace,
		Name:        object.Name,
	}, w.Logger, object)
	crd := object.ToAppCrd()
	app := model.Application{
		Cluster: w.RegionID,
		Crd:     *object,
	}
	resources, err := s.GetAppSubResources(crd)
	if err != nil {
		return err
	}
	workloads := funk.Filter(resources, func(x *model.KubernetesObject) bool { return common.IsPodController(x.Kind) }).([]*model.KubernetesObject)
	for _, item := range workloads {
		replicas := item.GetAnnotations()[common.AppLastReplicasKey()]
		item.SetPodReplicas(cast.ToFloat64(replicas))
	}
	if err := kubeStore.CreateApplicationObjects(&app, workloads); err != nil {
		w.SendApplicationWarningEvent(object, err)
		return err
	}
	w.SendApplicationNormalEvent(object, "Application pods have been started", common.ReasonStarted)
	return kubeStore.PatchApplication(&app, model.GenSimpleApplicationUpdateData(string(v1alpha1.Succeeded),
		common.AppActionKey(), "start"))
}

// StopApplication will trying to `stop` a application by scale down all the workloads.
// To do so, we need to keep track the origin replicas of the workload and.
func StopApplication(object *model.KubernetesObject, w *RegionWatcher) error {
	s := kubeStore.GetKubernetesResourceStore(&model.Query{
		ClusterUUID: w.RegionID,
		Namespace:   object.Namespace,
		Name:        object.Name,
	}, w.Logger, object)
	crd := object.ToAppCrd()
	app := model.Application{
		Cluster: w.RegionID,
		Crd:     *object,
	}
	resources, err := s.GetAppSubResources(crd)
	if err != nil {
		return err
	}

	workloads := funk.Filter(resources, func(x *model.KubernetesObject) bool { return common.IsPodController(x.Kind) }).([]*model.KubernetesObject)
	for _, item := range workloads {
		replicas, _ := item.GetPodReplicas()
		item.SetAno(common.AppLastReplicasKey(), cast.ToString(replicas))
		item.SetPodReplicas(0)
	}
	if err := kubeStore.CreateApplicationObjects(&app, workloads); err != nil {
		w.SendApplicationWarningEvent(object, err)
		return err
	}
	w.SendApplicationNormalEvent(object, "Application pods have been stopped", common.ReasonStopped)
	return kubeStore.PatchApplication(&app, model.GenSimpleApplicationUpdateData(string(v1alpha1.Succeeded),
		common.AppActionKey(), "stop"))
}

func (a *ApplicationEventHandler) UpdateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	uuid := object.Labels[common.AppUidKey()]
	logger := w.Logger.WithField("app", uuid)

	locker, err := w.RedisClient.GetLocker(uuid)
	if err != nil {
		return err
	}
	defer locker.Unlock()

	if object.IsAppStoppingAction() {
		return StopApplication(object, w)
	}

	if object.IsAppStartingAction() {
		return StartApplication(object, w)
	}

	appCrd := object.ToAppCrd()
	if appCrd.Spec.AssemblyPhase != v1alpha1.Pending {
		return nil
	}

	status, app, err := extractStatus(w, uuid)
	if err != nil {
		return err
	}

	deletes := model.KubernetesObjectList{}
	if err := json.Unmarshal([]byte(status.Input["deleted"]), &deletes); err != nil {
		return err
	}

	w.SendApplicationNormalEvent(object, "Preparing to update application sub resources", common.ReasonUpdating)

	model.SortKubernetesObjects(app.Kubernetes, false)
	if err := kubeStore.CreateApplicationObjects(app, app.Kubernetes); err != nil {
		logger.Errorf("Create app sub-resource %v error: %v.", app.GetUUID(), err.Error())
		w.SendApplicationWarningEvent(object, err)
		return UpdateApplicationPhase(app, v1alpha1.Failed)
	}

	if err := kubeStore.DeleteApplicationObjects(app, deletes.Items); err != nil {
		logger.Errorf("Delete app sub-resource %v error: %v.", app.GetUUID(), err.Error())
		w.SendApplicationWarningEvent(object, err)
		return UpdateApplicationPhase(app, v1alpha1.Failed)
	}
	w.SendApplicationNormalEvent(object, "All sub resources have been synced", common.ReasonSynced)
	return UpdateApplicationPhase(app, v1alpha1.Succeeded)
}

func (w *RegionWatcher) SendApplicationWarningEvent(object *model.KubernetesObject, err error) {

	ev := model.Event{
		Message: err.Error(),
		Type:    common.EventTypeWarning,
		Reason:  common.FailedSync,
		UID:     string(object.UID),
	}
	w.sendEvent(object, &ev)
}

func (w *RegionWatcher) SendApplicationNormalEvent(object *model.KubernetesObject, message, reason string) {

	ev := model.Event{
		Message: message,
		Type:    common.EventTypeNormal,
		Reason:  reason,
		UID:     string(object.UID),
	}
	w.sendEvent(object, &ev)
}

func (w *RegionWatcher) sendEvent(object *model.KubernetesObject, ev *model.Event) {
	app := model.Application{
		Cluster: w.RegionID,
		Crd:     *object,
	}

	if newErr := kubeStore.SendKubernetesApplicationEvent(&app, ev); newErr != nil {
		w.Logger.WithField("app", app.GetUUID()).WithError(newErr).Error("send event error")
	}

}

type CRDEventHandler struct {
	BaseEventHandler
}

func (c *CRDEventHandler) CreateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	crd, err := object.ToCRD()
	if err != nil {
		return err
	}

	w.InvalidateApiDiscoveryCache()
	w.Logger.Infof("New CRD created, try to watch the '%s' resource", crd.Spec.Names.Plural)
	w.watchUnregisteredResource(crd.Spec.Names.Plural, schema.GroupVersionKind{
		Group:   crd.Spec.Group,
		Version: crd.Spec.Version,
		Kind:    crd.Spec.Names.Kind,
	})

	return nil
}

func (c *CRDEventHandler) DeleteHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	crd, err := object.ToCRD()
	if err != nil {
		return err
	}

	w.InvalidateApiDiscoveryCache()
	w.Logger.Infof("CRD '%s' deleted, try to stop watch the resource", crd.Spec.Names.Plural)
	w.StopWatchResource(crd.Spec.Names.Plural)

	return nil
}

type NamespaceEventHandler struct {
	BaseEventHandler
}

func (c *NamespaceEventHandler) CreateHandler(object *model.KubernetesObject, w *RegionWatcher) error {
	if object.Name == "default" {
		w.Logger.Info("receive default namespace add event, trying to sync with alauda resources")
		if _, err := infra.GetResourceFromJakiro(string(object.UID)); err != nil {
			if errors.Cause(err) == common.ErrResourceNotExist {
				regionResource, err := infra.GetResourceFromJakiro(w.KubeClient.RegionID)
				if err != nil {
					w.Logger.WithError(err).Warnf("Get region data error")
					return err
				}

				// get project uuid
				project, err := infra.GetResourceFromJakiroByName(
					regionResource.Namespace,
					"PROJECT",
					config.GlobalConfig.Platform.DefaultProject,
				)
				if err != nil {
					w.Logger.WithError(err).Error("get default project from jakiro error")
					return err
				}

				body := &model.AlaudaInternalResource{
					Type:        "NAMESPACE",
					Name:        fmt.Sprintf("%s:%s", w.KubeClient.RegionID, object.Name),
					UUID:        string(object.UID),
					RegionID:    w.KubeClient.RegionID,
					CreatedBy:   regionResource.CreatedBy,
					Namespace:   regionResource.Namespace,
					ProjectUUID: project.UUID,
				}

				if err := infra.CreateJakiroResource(body); err != nil {
					w.Logger.WithError(err).Warnf("Create default namespace resource error")
				}
			} else {
				w.Logger.Info("default namespace already created!")
			}
		}
	}
	return nil
}
