package handler

import (
	"encoding/json"
	"net/http"

	"vision/db"
	"vision/infra"

	"github.com/alauda/bergamot/diagnose"
)

func Ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("vision: Humans are odd. ..."))
}

func Diagnose(w http.ResponseWriter, r *http.Request) {
	reporter, _ := diagnose.New()
	reporter.Add(db.NewDBChecker(db.GoQuDB))
	w.Header().Set("Content-Type", "application/json")

	var report diagnose.HealthReport

	redis, err := infra.GetRedis()
	if err == nil {
		reporter.Add(redis)
		report = reporter.Check()
	} else {
		result := reporter.Check()
		redisReport := diagnose.NewReport("redis")
		redisReport.Message = err.Error()
		redisReport.Status = diagnose.StatusError
		result.Add(*redisReport)
		result.Status = diagnose.StatusError
		report = result
	}

	data, _ := json.Marshal(report)
	w.Write(data)

}
