package common

import (
	"fmt"
	"time"

	"github.com/davecgh/go-spew/spew"
	"k8s.io/apimachinery/pkg/util/rand"
	"k8s.io/apimachinery/pkg/util/uuid"
)

func ToString(data interface{}) string {
	return spew.Sprintf("%+v", data)
}

// GetTimeDuration generate a time duration of seconds
func GetTimeDuration(t int) time.Duration {
	return time.Duration(t) * time.Second
}

// StringInSlice check whether a string is in an slice.
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// RemoveEmptyString remove empty string for a slice.
func RemoveEmptyString(list []string) []string {
	var result []string
	for _, v := range list {
		if v == "" {
			continue
		} else {
			result = append(result, v)
		}
	}
	return result
}

func GetCurrentTimeStamp() int64 {
	return time.Now().Unix()
}

func TimeStampToTime(timestamp int64) time.Time {
	return time.Unix(timestamp, 0)
}

// NewUUID generate a new uuid
func NewUUID() string {
	return string(uuid.NewUUID())
}

// CombineString is a dump string combine function (by : )
func CombineString(items ...string) string {
	str := ""
	for _, item := range items {
		if str == "" {
			str = item
		} else {
			str = fmt.Sprintf("%s:%s", str, item)
		}
	}
	return str
}

// GenerateRandString will return a fixed length random string
func GenerateRandString(n int) string {
	return rand.String(n)
}
