package common

import (
	"strings"

	"github.com/juju/errors"
)

// internal errors
var (
	ErrCacheMiss = errors.New("cache miss")
	// used for db
	ErrResourceNotExist = errors.New("resource not exist")
	// used for cache
	ErrNotFound = errors.New("resource not found")

	ErrKubernetesResourceExists = errors.New("kubernetes resource already exist")
)

const (
	// NoRowsError is returned by Scan when QueryRow doesn't return a row. In such a case,
	// QueryRow returns a placeholder *Row value that defers this error until a Scan.
	// Note: this may be only used for squirrel package, may be useless for now
	NoRowsError = "sql: no rows in result set"
	// RedisNotFoundError is returned by redis if cache miss.
	RedisNotFoundError = "redis: nil"
)

func IsNotExistError(err error) bool {
	if err == nil {
		return false
	}
	return errors.Cause(err) == ErrResourceNotExist || strings.Contains(err.Error(), NoRowsError) || strings.Contains(err.Error(), RedisNotFoundError)
}
