package common

import (
	log "github.com/sirupsen/logrus"
)

type Log struct {
	*log.Entry
}

func ToLog(entry *log.Entry) Log {
	return Log{entry}
}

func GetLoggerByServiceID(serviceID string) Log {
	return Log{log.WithFields(log.Fields{"service_id": serviceID})}
}

func GetLoggerByRegionID(regionID string) Log {
	return Log{log.WithFields(log.Fields{"region": regionID})}
}

func GetLoggerByKV(k, v string) Log {
	return Log{log.WithFields(log.Fields{k: v})}
}
