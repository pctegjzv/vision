package common

import (
	"strings"

	"vision/config"
)

// Kubernetes API version
const (
	KubernetesAPIVersionV1 = "v1"
)

// Kubernetes Resources
const (
	KubernetesKindNamespace   = "Namespace"
	KubernetesKindApplication = "Application"
	KubernetesKindService     = "Service"
	KubernetesKindDeployment  = "Deployment"
	KubernetesKindDaemonSet   = "DaemonSet"
	KubernetesKindStatefulSet = "StatefulSet"

	KubernetesKindPVC   = "PersistentVolumeClaim"
	KubernetesKindEvent = "Event"
)

// Kubernetes Revisions
const (
	RevisionAnnotation    = "deployment.kubernetes.io/revision"
	ChangeCauseAnnotation = "kubernetes.io/change-cause"
)

// Kubernetes Event Reasons
const (
	FailedSync     = "FailedSync"
	ReasonSynced   = "Synced"
	ReasonStarted  = "Started"
	ReasonStopped  = "Stopped"
	ReasonUpdating = "Updating"
)

// kubernetes api params
const (
	ParamLabelSelector = "labelSelector"
)

// kubernetes event
const (
	EventTypeNormal  = "Normal"
	EventTypeWarning = "Warning"
)

// Kubernetes topology reference kinds
const (
	Reference = "Reference"
	Selector  = "Selector"
)

var (
	ClusterScopeResourceKind = map[string]bool{
		KubernetesKindNamespace: true,
		"PersistentVolume":      true,
		"StorageClass":          true,
		"ClusterRole":           true,
		"ClusterRoleBinding":    true,
	}
)

// GetKubernetesTypeFromKind get kubernetes type by kind.
// eg: DaemonSet --> daemonsets
// current support:
// 1. Deployment / DaemonSet / StatefulSet
// TODO: handler special case
func GetKubernetesTypeFromKind(kind string) string {
	switch kind {
	case "NetworkPolicy":
		return "networkpolicies"
	case "Ingress":
		return "ingresses"
	case "StorageClass":
		return "storageclasses"
	default:
		return strings.ToLower(kind) + "s"
	}
}

// Kubernetes Deploy Mode. (Pod controllers)
var (
	KubernetesDeployModeKind = map[string]bool{
		KubernetesKindDeployment:  true,
		KubernetesKindDaemonSet:   true,
		KubernetesKindStatefulSet: true,
	}
)

// IsPodController check whether a kubernetes resource kind is a pod controller.
// current support: daemonset/deployment/statefulset
func IsPodController(kind string) bool {
	return KubernetesDeployModeKind[kind]
}

func getKey(resource, attr string) string {
	return resource + "." + config.GlobalConfig.Label.BaseDomain + "/" + attr
}

func AppNameKey() string {
	return getAppKey("name")
}

func AppUidKey() string {
	return getAppKey("uuid")
}

func ResourceUidKey() string {
	return getKey("resource", "uuid")
}

func getAppKey(attr string) string {
	return getKey("app", attr)
}

func AppCreateMethodKey() string {
	return getAppKey("create_method")
}

func AppSourceKey() string {
	return getAppKey("source")
}

func AppDeletedAtKey() string {
	return getAppKey("deleted_at")
}

// start/stop/starting/stopping
// 1. staring/stopping -> handle it
// 2. start/stop -> what can we do(stop or start) on this app.
func AppActionKey() string {
	return getAppKey("action")
}

func AppLastReplicasKey() string {
	return getAppKey("last-replicas")
}

func AppMigrationKey() string {
	return getAppKey("migration")
}
