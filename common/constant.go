package common

const (
	Component = "vision"
)

// Cache key prefix
const (
	// RegionWatcherKey is a HashMap, region id is th key, start timestamp is the value
	// redis-cli example: HSET krobelus:watcher:regions open-source-dev-cluster 1534845861
	RegionWatcherKey = "watcher:regions"

	// WatcherInstancesCacheKey stores vision-watcher instances info
	WatcherInstancesCacheKey = "meta:watcher_instances"
	// ClusterServiceClassesCachePrefix     = "v1beta1:broker_classes"
	// KubernetesResourceUIDCacheKeyPrefix is the key prefix for kubernetes resource stored by uuid
	// TODO(xxhe): Should be removed if v3 API have migrated to use name as key.
	KubernetesResourceUIDCacheKeyPrefix = "k8s:uid"
	// KubernetesResourceNameCacheKeyPrefix is the key prefix for kubernetes resource stored by name
	KubernetesResourceNameCacheKeyPrefix = "k8s:name"
	// KubernetesAPIDiscoveryCacheKeyPrefixFormat is the key format for kubernetes discovery cache.
	KubernetesAPIDiscoveryCacheKeyPrefixFormat = "k8s:%s:api:discovery"
)

// Default Query Params for paginate

// Service Status

type ResourceTargetState string

const (
	// Target states
	TargetStartedState ResourceTargetState = "Started"
	TargetStoppedState ResourceTargetState = "Stopped"
	TargetDeletedState ResourceTargetState = "Deleted"
	// only used by PV now.
	TargetCreatedState ResourceTargetState = "Created"
	// TargetUpdatedState ResourceTargetState = "Updated"
)

type ResourceStatus string

// Common status for apps/services/...
const (
	StatusRunning   ResourceStatus = "Running"
	StatusError     ResourceStatus = "Error"
	StatusDeploying ResourceStatus = "Deploying"
	StatusWarning   ResourceStatus = "Warning"
	StatusStopped   ResourceStatus = "Stopped"
	StatusUnknown   ResourceStatus = "Unknown"

	// StatusReleased  ResourceStatus = "Released"
	// StatusFailed    ResourceStatus = "Failed"

	//PVC status
	// StatusPending ResourceStatus = "Pending"
	// StatusLost    ResourceStatus = "Lost"
)

var (
	// RunningStatusList   = []KubernetesResourceStatus{CurrentCreatedStatus, CurrentUpdatedStatus}
	ErrorStatusList     = []KubernetesResourceStatus{CurrentCreateErrorStatus, CurrentUpdateErrorStatus, CurrentDeleteErrorStatus}
	DeployingStatusList = []KubernetesResourceStatus{CurrentCreatingStatus, CurrentUpdatingStatus, CurrentDeletingStatus}
	DeployedStatusList  = []KubernetesResourceStatus{CurrentCreatedStatus, CurrentUpdatedStatus, CurrentDeletedStatus}
)

type KubernetesResourceStatus string

const (
	CurrentCreatingStatus    KubernetesResourceStatus = "Creating"
	CurrentCreateErrorStatus KubernetesResourceStatus = "CreateError"
	CurrentCreatedStatus     KubernetesResourceStatus = "Created"
	CurrentUpdatingStatus    KubernetesResourceStatus = "Updating"
	CurrentUpdateErrorStatus KubernetesResourceStatus = "UpdateError"
	CurrentUpdatedStatus     KubernetesResourceStatus = "Updated"
	CurrentDeletingStatus    KubernetesResourceStatus = "Deleting"
	CurrentDeleteErrorStatus KubernetesResourceStatus = "DeleteError"
	CurrentDeletedStatus     KubernetesResourceStatus = "Deleted"
)

func (s KubernetesResourceStatus) IsDeployingStatus() bool {
	for _, item := range DeployingStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeployedStatus() bool {
	for _, item := range DeployedStatusList {
		if s == item {
			return true
		}
	}
	return false
}
func (s KubernetesResourceStatus) IsDeployErrorStatus() bool {
	for _, item := range ErrorStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeleting() bool {
	return s == CurrentDeletingStatus
}

func (target ResourceTargetState) IsDeleted() bool {
	return target == TargetDeletedState
}

func (target ResourceTargetState) IsStopped() bool {
	return target == TargetStoppedState
}

// DB drivers

// Alauda Volume Types
const (
	Glusterfs = "glusterfs"
	EBS       = "ebs"
)

const VolumeStateAvailable = "available"

const (
	HttpGet    = "GET"
	HttpPost   = "POST"
	HttpPut    = "PUT"
	HttpDelete = "DELETE"
	HttpPatch  = "PATCH"
)

type ServiceScaleAction string

const (
	ScaleUp   ServiceScaleAction = "scale-up"
	ScaleDown ServiceScaleAction = "scale-down"
)

const (
	NamespaceDefault = "default"
)
