package db

import (
	"database/sql"
	"encoding/json"
	"time"

	"vision/common"
	"vision/model"

	"github.com/juju/errors"
	"gopkg.in/doug-martin/goqu.v4"
)

const (
	TypeField    = "type"
	QueueIDField = "queue_id"
	OutputField  = "output"
	InputField   = "input"
)

type StatusStore struct {
	Status *model.AlaudaResourceStatus
	Logger common.Log
}

type StatusRow struct {
	UUID         string         `db:"uuid"`
	Type         string         `db:"type"`
	CurrentState string         `db:"current_state"`
	TargetState  string         `db:"target_state"`
	QueueID      string         `db:"queue_id"`
	Output       sql.NullString `db:"output"`
	Input        string         `db:"input"`
	CreatedAt    time.Time      `db:"created_at"`
	UpdatedAt    time.Time      `db:"updated_at"`
}

func (store *StatusStore) ToRecord() goqu.Record {
	params, err := json.Marshal(store.Status.Input)
	if err != nil {
		panic(err.Error())
	}
	return goqu.Record{
		UUIDField:         store.Status.UUID,
		TypeField:         store.Status.Type,
		CurrentStateField: store.Status.CurrentState,
		TargetStateField:  store.Status.TargetState,
		InputField:        string(params),
		QueueIDField:      store.Status.QueueID,
		OutputField:       store.Status.Output,
	}
}

func (row *StatusRow) ToResponse() *model.AlaudaResourceStatus {
	params := map[string]string{}
	if err := json.Unmarshal([]byte(row.Input), &params); err != nil {
		panic(err.Error())
	}
	return &model.AlaudaResourceStatus{
		UUID:         row.UUID,
		Type:         model.ResourceType(row.Type),
		CurrentState: common.KubernetesResourceStatus(row.CurrentState),
		TargetState:  common.ResourceTargetState(row.TargetState),
		QueueID:      row.QueueID,
		Output:       NullStringToDefaultString(row.Output),
		Input:        params,
		CreatedAt:    row.CreatedAt,
		UpdatedAt:    row.UpdatedAt,
	}
}

func (store *StatusStore) Retrieve(eq goqu.Ex) (*model.AlaudaResourceStatus, error) {
	row := StatusRow{}
	result, err := GoQuDB.From(StatusTable).Where(eq).ScanStruct(&row)
	if err != nil {
		return nil, errors.Annotate(err, "get status")
	}
	if !result {
		return nil, errors.Annotate(common.ErrResourceNotExist, "get status")
	}
	return row.ToResponse(), nil
}

func GetStatus(uuid string) (*model.AlaudaResourceStatus, error) {
	store := StatusStore{
		Status: &model.AlaudaResourceStatus{UUID: uuid},
		Logger: common.GetLoggerByServiceID(uuid),
	}
	return store.Retrieve(goqu.Ex{UUIDField: store.Status.UUID})
}
