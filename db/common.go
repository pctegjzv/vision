package db

import (
	"database/sql"
)

// GetUUIDS ..
type GetUUIDS struct {
	UUIDS []string `json:"uuids"`
}

// ResourceBase ..
type ResourceBase struct {
	UUID string `json:"uuid" binding:"required"`
	Name string `json:"name" binding:"required"`
}

type ResourceNameBase struct {
	Name string `json:"name" binding:"required"`
	UUID string `json:"uuid,omitempty"`
}

type Namespace ResourceBase

type Cluster ResourceBase

type ResourceBaseWithType struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

type Parent ResourceBaseWithType

type ReferencedResource ResourceBaseWithType

type ReferencedResources []ReferencedResource

type JsonObject map[string]interface{}

const (
	StatusTable = "vision_status"
)

// common db fields
const (
	UUIDField = "uuid"

	CurrentStateField = "current_state"
	TargetStateField  = "target_state"
)

type ResourceStatus string

const (
	dbDriverPostgres = "postgresql"
	dbDriverMysql    = "mysql"
)

// NullStringToDefaultString translate a sql NullString to a string. If it is NUll, set it to ""
func NullStringToDefaultString(n sql.NullString) string {
	if n.Valid {
		return n.String
	}
	return ""
}

// IsAlreadyExist is a interface that used for resource create. when we received a create request,  sometimes
// we need to check the resource is not exist in db. Most of the *Request struct should implements this interface.
// Note: implements the interface in *Store (ensure *Request exist)
type IsAlreadyExist interface {
	// IsAlreadyExist check if a target resource already exist in db
	IsAlreadyExist() (bool, error)
}
