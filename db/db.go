package db

import (
	"os"

	"vision/common"
	"vision/config"

	"time"

	"github.com/alauda/bergamot/db"
	"github.com/alauda/bergamot/diagnose"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gopkg.in/doug-martin/goqu.v4"
	_ "gopkg.in/doug-martin/goqu.v4/adapters/mysql"
	_ "gopkg.in/doug-martin/goqu.v4/adapters/postgres"
	"k8s.io/apimachinery/pkg/util/wait"
)

var (
	GoQuDB *goqu.Database
)

func IsPostgres() bool {
	return config.GlobalConfig.DB.Engine == dbDriverPostgres
}

func IsMysql() bool {
	return config.GlobalConfig.DB.Engine == dbDriverMysql
}

func InitDB() {
	// ignore errors before reach timeout
	condition := func() (done bool, err error) {
		logrus.Debugf("Connecting to database...")
		database, err := GetDB()
		if err != nil {
			logrus.WithError(err).Warn("Connect to db error")
			return false, nil
		}
		logrus.Infof("Connect to database done: %s", database.Dialect)
		GoQuDB = database
		return true, nil
	}

	err := wait.Poll(common.GetTimeDuration(1), common.GetTimeDuration(10), condition)
	if err != nil {
		logrus.WithError(err).Info("Connect db error, exit program")
		os.Exit(int(2))
	}
}

// DatabaseChecker simple database checker for application
type DatabaseChecker struct {
	db *goqu.Database
}

// NewChecker constructor
func NewDBChecker(db *goqu.Database) *DatabaseChecker {
	return &DatabaseChecker{
		db: db,
	}
}

// Diagnose start diagnose check
// http://confluence.alaudatech.com/pages/viewpage.action?pageId=14123161
func (d *DatabaseChecker) Diagnose() diagnose.ComponentReport {
	var (
		err   error
		start time.Time
	)

	report := diagnose.NewReport("database")
	start = time.Now()

	// does not work
	// err = d.db.Db.Ping()

	// this works
	tx, err := d.db.Db.Begin()
	if tx != nil {
		tx.Rollback()
	}
	report.Check(err, "Database ping failed", "Check environment variables or database health")
	report.AddLatency(start)
	return *report
}

func getDBConfig() db.DatabaseConnectionOpts {
	cfg := config.GlobalConfig.DB
	return *db.NewDatabaseConnectionOpts(
		cfg.Host,
		cfg.Name,
		cfg.User,
		cfg.Password,
		cfg.Port,
		cfg.Timeout,
		cfg.MaxConn,
		cfg.MaxIdleConn,
	)
}

func getDBEngine() db.Engine {
	switch config.GlobalConfig.DB.Engine {
	case "mysql":
		return db.MySQL
	default:
		return db.Postgres
	}
}

// GetDB return a database connection or error
func GetDB() (*goqu.Database, error) {
	return db.New(getDBEngine(), getDBConfig())
}
