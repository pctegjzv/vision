#!/bin/bash
PACKAGE=$1
if [ "$PACKAGE" = "" ]; then
    PACKAGE="domain"
fi
echo $PACKAGE
go test -coverprofile $PACKAGE".out" "./"$PACKAGE
go tool cover -html=$PACKAGE".out" -o $PACKAGE".html"
if [ "$2" = "" ]; then
    open $PACKAGE".html"
fi
 
