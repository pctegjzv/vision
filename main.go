package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"

	"vision/config"
	"vision/db"
	"vision/handler"
	"vision/watcher"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

func init() {
	// Register the summary and the histogram with Prometheus's default registry.
	prometheus.MustRegister(
		watcher.EventCounter,
		watcher.EventBytesCounter,
		watcher.EventDeleteCounter,
		watcher.EventUpdateCounter,
		watcher.EventAddCounter,
	)
}

func main() {
	config.InitConfig()
	config.SetLogger()

	logrus.Debugf("Load env: %+v", config.GlobalConfig)

	db.InitDB()
	Watcher()
}

func registerHandlers() {
	http.HandleFunc("/", handler.Ping)
	http.HandleFunc("/_ping", handler.Ping)
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/_diagnose", handler.Diagnose)
}

func Watcher() {
	logrus.Info("vision start")

	kw, err := watcher.NewKubeWatcher()
	if err != nil {
		return
	}
	err = kw.RegisterSelf()
	if err != nil {
		return
	}
	logrus.Info("register self")

	shutdown := make(chan int)
	sigChan := make(chan os.Signal, 1)

	go func() {
		registerHandlers()
		addr := fmt.Sprintf(":%d", config.GlobalConfig.Vision.Port)
		logrus.Println(http.ListenAndServe(addr, nil))
	}()

	go func() {
		kw.WatchRegions()
		shutdown <- 1
	}()
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-sigChan
		fmt.Println("Shutting down...")
		kw.CleanUp()
	}()
	<-shutdown
}
