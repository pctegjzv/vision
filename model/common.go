package model

// alauda resource type
type ResourceType string

const (
	AlaudaServiceType ResourceType = "AlaudaService"
	ApplicationType   ResourceType = "Application"
	NamespaceType     ResourceType = "Namespace"

	SecretType ResourceType = "Secret"

	LimitRange ResourceType = "LimitRange"
)

type ResourceStatus string
