package model

import (
	"encoding/json"
	"time"

	"vision/common"

	"github.com/juju/errors"
)

type AlaudaResourceStatus struct {
	UUID         string
	Type         ResourceType
	CurrentState common.KubernetesResourceStatus
	TargetState  common.ResourceTargetState
	QueueID      string
	Output       string
	Input        map[string]string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func (s *AlaudaResourceStatus) ToApplication() (*Application, error) {
	if s.Input == nil {
		return nil, errors.New("status input is nil")
	}
	var crd KubernetesObject
	if err := json.Unmarshal([]byte(s.Input["crd"]), &crd); err != nil {
		return nil, err
	}

	var objs []*KubernetesObject
	if err := json.Unmarshal([]byte(s.Input["kubernetes"]), &objs); err != nil {
		return nil, err
	}
	return &Application{
		Cluster:    s.Input["cluster"],
		Crd:        crd,
		Kubernetes: objs,
	}, nil

}

func (s *AlaudaResourceStatus) UpdateApplicationKubernetes(objects []*KubernetesObject) error {
	bytes, err := json.Marshal(objects)
	if err != nil {
		return err
	}
	s.Input["kubernetes"] = string(bytes)
	return nil
}
