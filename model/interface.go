package model

import (
	"fmt"

	"vision/common"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

type AlaudaResource interface {
	GetClusterUUID() string
	GetNamespace() string
	GetName() string
	GetLogger() common.Log
	GetUUID() string
	GetType() ResourceType
	// I'm your father!
	IsFamily(object v1.Object) bool
	GetLabelSelector() string
}

func (application *Application) GetClusterUUID() string {
	return application.Cluster
}

func (application *Application) GetNamespace() string {
	return application.Crd.Namespace
}

func (application *Application) GetName() string {
	return application.Crd.Name
}

func (application *Application) GetUUID() string {
	return GetAppUUID(&application.Crd)
}

func (application *Application) GetType() ResourceType {
	return ApplicationType
}

func (application *Application) GetLogger() common.Log {
	return common.GetLoggerByKV("application",
		fmt.Sprintf("%s:%s", application.GetNamespace(), application.Crd.Name))
}

func (application *Application) IsFamily(object v1.Object) bool {
	for key, value := range object.GetLabels() {
		if key == common.AppUidKey() {
			return value == application.GetUUID()
		}
	}
	return false
}

func (application *Application) GetLabelSelector() string {
	return common.AppUidKey() + "=" + application.GetUUID()
}
