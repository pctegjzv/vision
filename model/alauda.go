package model

import (
	"sort"
)

type KubernetesResourceObject struct {
	UUID        string
	Name        string
	NamespaceID string
	ResourceID  string
	APIVersion  string
	Kind        string
	Kubernetes  string
}

type AlaudaInternalResource struct {
	CreatedAt     string        `json:"created_at,omitempty"`
	CreatedBy     string        `json:"created_by"`
	Members       []interface{} `json:"members,omitempty"`
	Name          string        `json:"name"`
	Namespace     string        `json:"namespace"`
	NamespaceUUID string        `json:"namespace_uuid,omitempty"`
	ProjectUUID   string        `json:"project_uuid,omitempty"`
	RegionID      string        `json:"region_id"`
	SpaceUUID     string        `json:"space_uuid,omitempty"`
	Teams         []interface{} `json:"teams,omitempty"`
	Type          string        `json:"type"`
	UUID          string        `json:"uuid"`
}

var Kinds = []string{
	"Namespace",
	"Secret",
	"ConfigMap",
	"PersistentVolume",
	"ServiceAccount",
	"Pod",
	"ReplicationController",
	"Ingress",
	"Job",
	"PodDisruptionBudget",
	"HorizontalPodAutoscaler",
	"Service",
	"Deployment",
	"DaemonSet",
	"StatefulSet",
}

type ResourceSlice []*KubernetesObject

func (s ResourceSlice) Len() int {
	return len(s)
}

func (s ResourceSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s ResourceSlice) Less(i, j int) bool {
	x, y := len(Kinds), len(Kinds)
	for c, k := range Kinds {
		if k == s[i].Kind {
			x = c
		}
		if k == s[j].Kind {
			y = c
		}
	}
	return x < y
}

func SortKubernetesObjects(resources []*KubernetesObject, reverse bool) {
	if !reverse {
		sort.Sort(ResourceSlice(resources))
	} else {
		sort.Sort(sort.Reverse(ResourceSlice(resources)))
	}
}
