package model

import (
	"vision/common"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Query is build from url path param and query params.
// The common format should be :
// 1. /namespaces/<namespace>/<resource_type>/<resource_name>/<sub_resource_type>?cluster_uuid=<cluster_uuid>
// 2. /<resource_type>/<resource_name>?cluster_uud=<cluster_uuid>

type Query struct {
	ClusterUUID string `json:"cluster_uuid"`
	Namespace   string `json:"namespace"`
	Name        string `json:"name"`
	Type        string `json:"type"`

	SubType   string `json:"sub_type"`
	PatchType string `json:"patch_type"`
}

func GetQuery(cluster, kind string, object v1.Object) *Query {
	query := Query{
		ClusterUUID: cluster,
		Namespace:   object.GetNamespace(),
		Name:        object.GetName(),
		Type:        common.GetKubernetesTypeFromKind(kind),
		PatchType:   "application/merge-patch+json",
	}
	return &query
}
