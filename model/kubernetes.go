package model

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"vision/common"
	"vision/pkg/application/v1alpha1"

	"github.com/json-iterator/go"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	"k8s.io/api/core/v1"
	apiext "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

type JsonObject map[string]interface{}

type KubernetesSpec JsonObject

type KubernetesStatus JsonObject

// Note: use this instead KubernetesResource
// Note: still cannot parse Kind and ApiVersion
type KubernetesObject struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	Spec              map[string]interface{}   `json:"spec,omitempty"`
	Status            map[string]interface{}   `json:"status,omitempty"`
	Data              map[string]string        `json:"data,omitempty"`
	Rules             []map[string]interface{} `json:"rules,omitempty"`
	// For Secret
	StringData map[string]string `json:"stringData,omitempty"`
	// For Secret
	Type string `json:"type,omitempty"`
	//ClusterRoleBinding
	Subjects []map[string]interface{} `json:"subjects,omitempty"`
	// ClusterRoleBinding
	RoleRef map[string]interface{} `json:"roleRef,omitempty"`
	// Endpoints
	Subsets []map[string]interface{} `json:"subsets,omitempty"`
	// ServiceAccount
	Secrets                      []map[string]interface{} `json:"secrets,omitempty"`
	ImagePullSecrets             []map[string]interface{} `json:"imagePullSecrets,omitempty"`
	AutomountServiceAccountToken *bool                    `json:"automountServiceAccountToken,omitempty"`
	// StorageClass
	Provisioner          string            `json:"provisioner,omitempty"`
	Parameters           map[string]string `json:"parameters,omitempty"`
	ReclaimPolicy        *string           `json:"reclaimPolicy,omitempty"`
	MountOptions         []string          `json:"mountOptions,omitempty"`
	AllowVolumeExpansion *bool             `json:"allowVolumeExpansion,omitempty"`
	// For Rollback
	RollbackTo map[string]interface{} `json:"rollbackTo,omitempty"`
	// Items. for <Kind>List data
	Items []map[string]interface{} `json:"items,omitempty"`

	// For Event
	Count          int32                  `json:"count,omitempty"`
	Reason         string                 `json:"reason,omitempty"`
	Message        string                 `json:"message,omitempty"`
	Source         map[string]interface{} `json:"source,omitempty"`
	InvolvedObject map[string]interface{} `json:"involvedObject,omitempty"`
	FirstTimestamp metaV1.Time            `json:"firstTimestamp,omitempty"`
	LastTimestamp  metaV1.Time            `json:"lastTimestamp,omitempty"`
}

// KubernetesObjectList describes a struct for kubernetes object list.
type KubernetesObjectList struct {
	Items      []*KubernetesObject `json:"items"`
	ApiVersion string              `json:"apiVersion"`
	Kind       string              `json:"kind"`
}

type Event struct {
	// filled by source
	Type    string
	Reason  string
	Message string

	// filled by AlaudaResource
	Name      string
	Namespace string
	UID       string
}

// Note: if KubernetesObject cannot include all fields, use this
type KubernetesObjectSpecial struct {
	KubernetesObject `json:",inline"`
	Name             string `json:"name,omitempty"`
}

// copy from auto-scaling api group,
type CrossVersionObjectReference struct {
	// Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
	Kind string `json:"kind" protobuf:"bytes,1,opt,name=kind"`
	// Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
	Name string `json:"name" protobuf:"bytes,2,opt,name=name"`
	// API version of the referent
	// +optional
	APIVersion string `json:"apiVersion,omitempty" protobuf:"bytes,3,opt,name=apiVersion"`
}

func UpdateAnnotations(object metaV1.Object, key, value string) {
	anno := object.GetAnnotations()
	if anno == nil {
		anno = make(map[string]string)
	}
	anno[key] = value
	object.SetAnnotations(anno)
}

func (ks *KubernetesObject) GetString() string {
	return fmt.Sprintf("%s/%s/%s", ks.Kind, ks.Namespace, ks.Name)
}

func (ksl *KubernetesObjectList) String() string {
	var itemsStr []string
	for _, item := range ksl.Items {
		itemsStr = append(itemsStr, fmt.Sprintf("%+v", *item))
	}
	return fmt.Sprintf("Items: [%s]", strings.Join(itemsStr, ","))
}

func GetResourceUidAnnotation(object metaV1.Object) string {
	return object.GetAnnotations()[common.ResourceUidKey()]
}

func GetAlaudaUID(object metaV1.Object) string {
	uid := GetResourceUidAnnotation(object)
	if uid == "" {
		uid = string(object.GetUID())
	}
	return uid
}

func (ks *KubernetesObject) MatchRequirements(reqs []labels.Requirement) bool {
	set := labels.Set(ks.Labels)
	for _, req := range reqs {
		if !req.Matches(set) {
			return false
		}
	}
	return true
}

func (ks *KubernetesObject) AddPodTemplateLabels(labels map[string]string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Add pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		podTemplateSpec.Labels = make(map[string]string)
	}
	for k, v := range labels {
		podTemplateSpec.Labels[k] = v
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesObject) RemovePodTemplateLabels(keys []string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Remove pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		return nil
	}
	for _, k := range keys {
		delete(podTemplateSpec.Labels, k)
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesObject) GetPodReplicas() (int, error) {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return 0, nil
	}
	replicas, ok := ks.Spec["replicas"].(float64)
	if !ok {
		return 1, errors.New("Parse replicas from resource spec error")
	}
	return cast.ToIntE(replicas)
}

func (ks *KubernetesObject) SetPodReplicas(rs float64) error {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return nil
	}
	ks.Spec["replicas"] = rs
	return nil
}

func (ks *KubernetesObject) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	data, ok := ks.Spec["template"].(map[string]interface{})
	if !ok {
		return nil, errors.New("No PodTemplateSpec found in resource spec")
	}
	template := KubernetesSpec(data)
	podTemplateSpec, err := template.GetPodTemplateSpec()
	if err != nil {
		return nil, err
	}
	return podTemplateSpec, nil
}

func (ks *KubernetesObject) GetServiceSpec() (*v1.ServiceSpec, error) {
	data, err := json.Marshal(ks.Spec)
	if err != nil {
		return nil, err
	}
	spec := v1.ServiceSpec{}
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesSpec) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	bt, err := json.Marshal(ks)
	if err != nil {
		return nil, err
	}
	ss := v1.PodTemplateSpec{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func ToInterfaceMap(object interface{}) (result map[string]interface{}, err error) {
	var bt []byte
	if bt, err = json.Marshal(object); err != nil {
		return nil, err
	}
	if err = json.Unmarshal(bt, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (ks *KubernetesObject) ToBytes() ([]byte, error) {
	if len(ks.RollbackTo) != 0 {
		return json.Marshal(KubernetesObjectSpecial{KubernetesObject: *ks, Name: ks.Name})
	}
	return json.Marshal(ks)
}

func BytesToKubernetesObject(bytes []byte) (*KubernetesObject, error) {
	var json = jsoniter.ConfigFastest
	var k KubernetesObject
	err := json.Unmarshal(bytes, &k)
	return &k, err
}

func BytesToKubernetesObjectList(bytes []byte) (*KubernetesObjectList, error) {
	var k KubernetesObjectList
	err := json.Unmarshal(bytes, &k)
	return &k, err
}

func (ks *KubernetesObject) ToApplication() (*v1alpha1.Application, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	obj := v1alpha1.Application{}
	err = json.Unmarshal(bt, &obj)
	obj.Kind = string(ApplicationType)
	obj.APIVersion = "app.k8s.io/v1alpha1"
	return &obj, err
}

func (ks *KubernetesObject) ToCRD() (*apiext.CustomResourceDefinition, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	obj := apiext.CustomResourceDefinition{}
	err = json.Unmarshal(bt, &obj)
	obj.Kind = string("CustomResourceDefinition")
	obj.APIVersion = "apiextensions.k8s.io/v1beta1"
	return &obj, err
}

func (ks *KubernetesObject) ToV1Service() (*v1.Service, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	service := v1.Service{}
	err = json.Unmarshal(bt, &service)
	return &service, err
}

func (ks *KubernetesObject) ParseV1Service(service *v1.Service) error {
	bt, err := json.Marshal(service)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bt, ks)
	if err != nil {
		return err
	}
	return nil
}

func ToKubernetesObject(object interface{}) (*KubernetesObject, error) {
	var res KubernetesObject
	data, err := json.Marshal(object)
	if err != nil {
		return &res, err
	}
	if err := json.Unmarshal(data, &res); err != nil {
		return nil, err
	} else {
		return &res, nil
	}
}

func RestResultToObject(result *rest.Result) (*KubernetesObject, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}
	return BytesToKubernetesObject(bt)
}

func AppCrdToObject(app *v1alpha1.Application) *KubernetesObject {
	bt, _ := json.Marshal(app)
	var obj KubernetesObject
	json.Unmarshal(bt, &obj)
	return &obj
}

// GenKubernetesApplicationEvent generate  a kubernetes application event
// Note: uuid means alauda uuid
func GenKubernetesApplicationEvent(ev *Event) v1.Event {
	event := v1.Event{
		TypeMeta: metaV1.TypeMeta{
			Kind:       common.KubernetesKindEvent,
			APIVersion: common.KubernetesAPIVersionV1,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ev.Name + "-" + common.GenerateRandString(5),
			Namespace: ev.Namespace,
		},
		InvolvedObject: v1.ObjectReference{
			Kind:      common.KubernetesKindApplication,
			Name:      ev.Name,
			Namespace: ev.Namespace,
			UID:       types.UID(ev.UID),
		},
		Message: ev.Message,
		Reason:  ev.Reason,
		Source: v1.EventSource{
			Component: common.Component,
		},
		Count:          1,
		Type:           ev.Type,
		FirstTimestamp: metaV1.Time{Time: time.Now().UTC()},
		LastTimestamp:  metaV1.Time{Time: time.Now().UTC()},
	}
	return event
}
