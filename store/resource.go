package store

import (
	"fmt"

	"vision/common"
	"vision/infra"
	"vision/model"

	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

// KubernetesResourceStore combine the ability to retrieve kubernetes resource from cache and cluster
type KubernetesResourceStore struct {
	ResourceStore
	Client rest.Interface
}

func (s *KubernetesResourceStore) String() string {
	return fmt.Sprintf("%s/%s/%s", s.UniqueName.Namespace, s.UniqueName.Type, s.UniqueName.Name)
}

func GetKubernetesResourceStore(query *model.Query, logger common.Log, object *model.KubernetesObject) KubernetesResourceStore {
	return KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: query,
			Logger:     logger,
			Object:     object,
		},
	}
}

func (s *KubernetesResourceStore) LoadResource(parent model.AlaudaResource, resource *model.KubernetesObject) {
	s.UniqueName = &model.Query{
		ClusterUUID: parent.GetClusterUUID(),
		Namespace:   parent.GetNamespace(),
		Type:        "",
		Name:        "",
	}
	s.Object = nil
	s.Logger = parent.GetLogger()

	if resource != nil {
		s.UniqueName.Type = common.GetKubernetesTypeFromKind(resource.Kind)
		s.UniqueName.Name = resource.Name
		s.Object = resource
	}
}

func (s *KubernetesResourceStore) ResourceTypeForObject(obj *model.KubernetesObject) (string, error) {
	gv, err := schema.ParseGroupVersion(obj.APIVersion)
	if err != nil {
		return "", err
	}

	return s.ResourceTypeForGVK(schema.GroupVersionKind{
		Group: gv.Group,
		Kind:  obj.Kind,
	})
}

func (s *KubernetesResourceStore) ResourceTypeForGVK(gvk schema.GroupVersionKind) (string, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return "", err
	}
	return client.ResourceTypeForGVK(gvk)
}

func (s *KubernetesResourceStore) LoadClient() error {
	s.Logger.Debugf("Load resource store client with query: %+v", s.UniqueName)
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return err
	}

	groupVersion := ""
	if s.Object != nil {
		groupVersion = s.Object.APIVersion
	}

	s.Client, err = client.ClientForGroupVersionResource(groupVersion, s.UniqueName.Type)
	if err != nil {
		return err
	}

	return nil
}

// SetRaw sets data bytes to 'Raw', and clear the 'Object' value.
func (s *KubernetesResourceStore) SetRaw(raw []byte) *KubernetesResourceStore {
	s.Raw = raw
	// Set Object to nil to avoid use Object as request body.
	s.Object = nil
	return s
}

func (s *KubernetesResourceStore) Request(method string, params map[string]string) (*rest.Result, error) {
	if err := s.LoadClient(); err != nil {
		return nil, err
	}
	var request *rest.Request
	switch method {
	case common.HttpPost:
		request = s.Client.Post()
	case common.HttpDelete:
		request = s.Client.Delete()
	case common.HttpPut:
		request = s.Client.Put()
	case common.HttpGet:
		request = s.Client.Get()
	case common.HttpPatch:
		request = s.Client.Patch(types.PatchType(s.UniqueName.PatchType))
	default:
		return nil, errors.New(fmt.Sprintf("Unsupported request method: %s", method))
	}
	request = request.Resource(s.UniqueName.Type)
	//TODO: fix default == "" bug
	if s.UniqueName.Namespace != "" {
		namespaceRequired := true
		if s.Object != nil {
			if _, exist := common.ClusterScopeResourceKind[s.Object.Kind]; exist {
				s.Logger.Infof("This is a cluster scope resource: %s", s.Object.Kind)
				namespaceRequired = false
			}
		}
		if namespaceRequired {
			request = request.Namespace(s.UniqueName.Namespace)
		}
	}
	if s.UniqueName.Name != "" && (method != common.HttpPost || s.UniqueName.SubType == "rollback") {
		request = request.Name(s.UniqueName.Name)
	}
	if s.UniqueName.SubType != "" {
		request = request.SubResource(s.UniqueName.SubType)
	}
	for key, value := range params {
		request = request.Param(key, value)
	}
	if method == common.HttpPost || method == common.HttpPut || method == common.HttpPatch {
		if s.Raw != nil {
			request.Body(s.Raw)
			s.Logger.Infof("Trying to create/update resource: %s", string(s.Raw))
		}
		if s.Object != nil {
			bytes, err := s.Object.ToBytes()
			if err != nil {
				return nil, err
			}
			s.Logger.Infof("Trying to create/update resource: %s", string(bytes))

			request.Body(bytes)
		}
	}
	s.Logger.Debugf("Request kubernetes: %s %s", method, request.URL().String())
	result := request.Do()
	if result.Error() != nil {
		s.Logger.Infof("Request kubernetes result error: %s", result.Error().Error())
	} else {
		var code int
		result.StatusCode(&code)
		s.Logger.Debugf("Request kubernetes result code: %d", code)
	}
	return &result, result.Error()
}

// FetchResource fetches a resource from cache or cluster.
func (s *KubernetesResourceStore) FetchResource() error {
	// Get from the cache first then from the cluster
	if err := s.LoadCacheObject(); err == nil {
		s.Logger.Debugf("Get resource %s in %s from cache", s.UniqueName.Type, s.UniqueName.ClusterUUID)
		return nil
	}

	result, err := s.Request("GET", nil)
	if result == nil && err != nil {
		s.Logger.Errorf("Get resource %s in %s failed: %s", s.UniqueName.Type, s.UniqueName.ClusterUUID, err.Error())
		return err
	}

	var code int
	result.StatusCode(&code)

	if result.Error() != nil {
		s.Logger.Infof("Request failed: %v %v ", code, result.Error())
		return ParseResultError(result)
	}

	s.Logger.Debugf("Request get status code: %d", code)
	return s.ParseResultData(result)
}

func ParseResultError(result *rest.Result) error {
	var code int
	result.StatusCode(&code)
	if code == 404 {
		return errors.Annotate(common.ErrResourceNotExist, result.Error().Error())
	}
	return result.Error()
}

func (s *KubernetesResourceStore) ParseResultData(result *rest.Result) error {
	raw, err := result.Raw()
	if err != nil {
		return err
	}
	s.Raw = raw
	s.Object, err = model.BytesToKubernetesObject(raw)
	return err
}

// FetchResourceList fetches resources from cache or cluster.
func (s *KubernetesResourceStore) FetchResourceList(params map[string]string) error {

	// Get from the cache first then from the cluster
	if err := s.LoadCacheObjects(params); err == nil {
		s.Logger.Debugf("Get resource list %s in %s from cache", s.UniqueName.Type, s.UniqueName.ClusterUUID)
		return nil
	}

	result, err := s.Request("GET", params)
	if result == nil && err != nil {
		s.Logger.Errorf("Get resource list %s in %s failed: %s", s.UniqueName.Type, s.UniqueName.ClusterUUID, err.Error())
		return err
	}

	if result.Error() != nil {
		s.Logger.Infof("Request failed: %v", result.Error())
		return result.Error()
	}

	var code int
	result.StatusCode(&code)
	s.Logger.Debugf("Request get status code: %d", code)
	raw, err := result.Raw()
	if err != nil {
		return err
	}
	s.Raw = raw
	s.Objects, err = model.BytesToKubernetesObjectList(raw)
	if err != nil {
		return err
	}
	return nil
}
