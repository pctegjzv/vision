package store

import (
	"vision/model"

	"k8s.io/client-go/rest"
)

type KubernetesStoreForTest struct {
}

func (s *KubernetesStoreForTest) GetChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) (*rest.Result, error) {
	return nil, nil
}

func (s *KubernetesStoreForTest) CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	return nil
}

func (s *KubernetesStoreForTest) UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	return nil
}

func (s *KubernetesStoreForTest) RollbackChildResource(parent model.AlaudaResource, resource *model.KubernetesObject, revision int64) error {
	return nil
}

func (s *KubernetesStoreForTest) DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	return nil
}
