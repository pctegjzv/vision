package store

import (
	"fmt"
	"strings"

	"vision/common"
	"vision/infra"
	"vision/model"

	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/labels"
)

type ResourceStore struct {
	// UUID is the uuid of the resource
	UUID string

	// UniqueName is a unique id for a resource
	UniqueName *model.Query

	// NameKey is a string represent of UniqueName
	NameKey string

	Object *model.KubernetesObject
	// Raw is the raw data of the kubernetes object(s)
	Raw     []byte
	Objects *model.KubernetesObjectList

	Logger common.Log

	// alauda error
	Error error
}

type ResourceListStore struct {
	UUIDs       []string
	ClusterUUID string
	Logger      common.Log
	Objects     []*model.KubernetesObject
}

// LoadCacheObject retrieve a single KubernetesObject from cache
func (s *ResourceStore) LoadCacheObject() error {
	if s.NameKey == "" && s.UniqueName != nil {
		s.NameKey = infra.ParseQueryToNameKey(s.UniqueName)
	}
	redis, err := infra.GetRedis()
	if err != nil {
		return err
	}
	data, err := redis.Get(s.NameKey)
	if err != nil {
		return errors.Annotate(err, "get resource from name cache error")
	}
	if data == "" && s.UniqueName != nil {
		return errors.Annotate(common.ErrNotFound,
			fmt.Sprintf("%s:%s", s.UniqueName.Type, s.UniqueName.Name))
	}
	s.Raw = []byte(data)
	ks, err := model.BytesToKubernetesObject([]byte(data))
	if err != nil {
		return errors.Annotate(err, "parse cached resource data error")
	}
	s.Object = ks
	return nil
}

// GetSelectorFromParams extract labelSelector param from params
func GetSelectorFromParams(params map[string]string) string {
	if params != nil && params[common.ParamLabelSelector] != "" {
		return params[common.ParamLabelSelector]
	}
	return ""

}

func CheckParamsForCache(params map[string]string) error {
	if params == nil || len(params) == 0 {
		return nil
	}
	if len(params) == 1 && params[common.ParamLabelSelector] != "" {
		return nil
	}
	return fmt.Errorf("unsupported params for cache list:%+v", params)
}

// LoadCacheObjects loads objects by resource type and namespace from cache.
// If no keys matched, return cache miss error.
// params: currently only support labelSelector
func (s *ResourceStore) LoadCacheObjects(params map[string]string) error {
	if err := CheckParamsForCache(params); err != nil {
		return err
	}

	s.Objects = &model.KubernetesObjectList{}
	selector := GetSelectorFromParams(params)
	requirements, err := labels.ParseToRequirements(selector)
	if err != nil {
		return err
	}
	keyMatch := infra.GetKubernetesCacheKeyMatch(s.UniqueName.ClusterUUID, s.UniqueName.Namespace, s.UniqueName.Type)
	var rawItems []string

	result, err := infra.GetMatchData(keyMatch)
	if err != nil {
		return errors.Annotate(err, "get resource list from cache error")
	}

	if len(result) == 0 {
		return fmt.Errorf("list from cache miss")
	}

	for _, val := range result {
		ks, err := model.BytesToKubernetesObject([]byte(val))
		if err != nil {
			return errors.Annotate(err, "parse cached resource data error")
		}
		if ks.MatchRequirements(requirements) {
			rawItems = append(rawItems, val)
			s.Objects.Items = append(s.Objects.Items, ks)
		}
	}

	s.Raw = []byte(fmt.Sprintf(`{"items":[%s]}`, strings.Join(rawItems, ",")))
	return nil
}

// LoadUniqueName will get resources's unique name from cache by uid if not present
func (s *ResourceStore) LoadUniqueName() error {
	if s.UniqueName != nil {
		if s.NameKey == "" {
			s.NameKey = infra.ParseQueryToNameKey(s.UniqueName)
		}
		return nil
	}
	redis, err := infra.GetRedis()
	if err != nil {
		return err
	}
	uidKey := infra.GetKubernetesResourceCacheKey(s.UUID)
	nameKey, err := redis.Get(uidKey)
	if err != nil {
		return errors.Annotate(err, "get resource name from uid cache error")
	}
	if nameKey == "" {
		return errors.Annotate(common.ErrNotFound, "get resource from cache error")
	}
	query := infra.ParseNameKeyToQuery(nameKey)
	s.UniqueName = query
	s.NameKey = nameKey
	return nil
}
