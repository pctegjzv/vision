package store

import (
	"fmt"

	"vision/common"
	"vision/config"
	"vision/infra"
	"vision/infra/kubernetes"
	"vision/model"

	"github.com/juju/errors"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/rest"
)

// KubernetesStore interface to interact with kubernetes cluster.
type KubernetesStore interface {
	GetChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) (*rest.Result, error)
	CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error
	UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error
	DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error
}

type KubernetesStoreForWorker struct {
	KubernetesResourceStore
}

func GenerateKubernetesStore() KubernetesStore {
	if config.GlobalConfig.Vision.DeployMode == "test" {
		return &KubernetesStoreForTest{}
	} else {
		return &KubernetesStoreForWorker{}
	}
}

func CreateApplicationObjects(parent model.AlaudaResource, objects []*model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	for _, object := range objects {
		if err := store.CreateChildResource(parent, object); err != nil {
			if err == common.ErrKubernetesResourceExists {
				if err := store.UpdateChildResource(parent, object); err != nil {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

func DeleteApplicationObjects(parent model.AlaudaResource, objects []*model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	for _, object := range objects {
		if err := store.DeleteChildResource(parent, object); err != nil {
			return err
		}
	}
	return nil
}

func DeleteApplication(app model.AlaudaResource, object *model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	if err := store.DeleteChildResource(app, object); err != nil {
		return err
	}
	return nil
}

// GetChildResource trying to get a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) GetChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) (*rest.Result, error) {
	s.LoadResource(parent, resource)
	s.Logger.Infof("Trying to retrieve %s in %s", resource.Name, resource.Namespace)
	return s.Request(common.HttpGet, nil)
}

// CreateChildResource trying to create a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	s.LoadResource(parent, resource)
	// Get kubernetes resource before create.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	// If resource not found, create it.
	if err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, create it")
			if _, err := s.Request(common.HttpPost, nil); err != nil {
				return errors.Annotate(err, prompt+"create error")
			}
			return nil
		} else {
			return errors.Annotate(err, prompt+"get error")
		}
	}

	// If resource exists, conflict error occurs.
	object, err := model.RestResultToObject(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	if !parent.IsFamily(object) {
		return errors.New(prompt + "already exists, but it does not belong to the same alauda resource")
	}
	return common.ErrKubernetesResourceExists
}

// UpdateChildResource trying to update a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	s.LoadResource(parent, resource)
	// If resource not found, error occurs.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	if err != nil {
		return err
	}

	// If resource does not belong to this service, skip.
	// Otherwise merge origin data if necessary.
	old, err := model.RestResultToObject(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	if !parent.IsFamily(old) {
		return errors.New(prompt + "already exists, but it does not belong to this alauda resource")
	}
	mergeOriginData(old, resource)

	// If resource already exists, update it.
	s.Object = resource
	if _, err := s.Request(common.HttpPut, nil); err != nil {
		return errors.Annotate(err, prompt+"update error")
	}
	return nil
}

// DeleteChildResource will delete a kubernetes resource if it really exists.
// If resource exists in cluster but does not belongs to same service, then skip delete.
func (s *KubernetesStoreForWorker) DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesObject) error {
	s.LoadResource(parent, resource)
	// Get kubernetes resource before delete.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	// If resource not found in cluster, skip.
	if err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, skip")
			return nil
		} else if err == kubernetes.ErrorResourceKindNotFound {
			s.Logger.Infof(prompt + " kind not found, skip")
			return nil
		} else {
			return errors.Annotate(err, prompt+"get error when delete resource")
		}
	}

	// If resource does not belong to this service, skip.
	object, err := model.RestResultToObject(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	uid := model.GetAlaudaUID(object)
	if !parent.IsFamily(object) {
		s.Logger.Infof(prompt + "does not belong to the same alauda resource, skip")
		return nil
	}

	// If resource belongs to this service, delete.
	if _, err := s.Request(common.HttpDelete, nil); err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, skip")
		} else {
			return errors.Annotate(err, prompt+"delete error")
		}
	}
	infra.DeleteResourceFromJakiro(uid)
	return nil
}

// mergeOriginData will merge exist data with the new one. for some resources, this is needed.
// such as PVC, when bounded, it's spec will changed(bad design).
func mergeOriginData(old *model.KubernetesObject, now *model.KubernetesObject) error {
	switch old.Kind {
	case common.KubernetesKindPVC:
		now.Spec = old.Spec
	case common.KubernetesKindService:
		oldService, err := old.ToV1Service()
		if err != nil {
			return err
		}
		nowService, err := now.ToV1Service()
		if err != nil {
			return err
		}
		nowService.Spec.ClusterIP = oldService.Spec.ClusterIP
		nowService.SetResourceVersion(oldService.ResourceVersion)
		return now.ParseV1Service(nowService)
	default:
		now.SetResourceVersion(old.ResourceVersion)
	}
	return nil
}

func SendKubernetesApplicationEvent(parent model.AlaudaResource, ev *model.Event) error {
	ev.Name = parent.GetName()
	ev.Namespace = parent.GetNamespace()
	// allow caller to custom uid
	if ev.UID == "" {
		ev.UID = parent.GetUUID()
	}

	event := model.GenKubernetesApplicationEvent(ev)
	resource, err := model.ToKubernetesObject(event)
	if err != nil {
		return err
	}
	return GenerateKubernetesStore().CreateChildResource(parent, resource)
}
