package store

import (
	"encoding/json"
	"fmt"
	"time"

	"vision/common"
	"vision/model"
	"vision/pkg/application/v1alpha1"

	"github.com/Jeffail/gabs"
	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

func (s *KubernetesResourceStore) InstallAppCrd() error {

	n := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: s.UniqueName.ClusterUUID,
				Name:        "",
				Namespace:   "",
				Type:        "customresourcedefinitions",
			},
			Logger: s.Logger,
		},
	}

	bt, err := json.Marshal(v1alpha1.ApplicationCRD)
	if err != nil {
		return errors.Annotate(err, "marshal app crd error")
	}
	obj, err := model.BytesToKubernetesObject(bt)
	if err != nil {
		return errors.Annotate(err, "load bytes to object error")
	}
	n.Object = obj
	_, err = n.Request("POST", nil)
	if err != nil {
		return err
	}
	n.Logger.Infof("Install app crd done")
	return nil
}

func (s *KubernetesResourceStore) GetApplication() (*model.Application, error) {
	err := s.FetchResource()
	if err != nil {
		return nil, err
	}

	app, err := s.Object.ToApplication()
	if err != nil {
		return nil, err
	}

	appModel := model.Application{
		Cluster: s.UniqueName.ClusterUUID,
		Crd:     *s.Object,
	}

	appModel.Kubernetes, err = s.GetAppSubResources(app)
	if err != nil {
		return nil, err
	}

	return &appModel, nil
}

func (s *KubernetesResourceStore) UpdateApplication(request *model.ApplicationUpdateRequest, oldApp *model.Application) (*rest.Result, error) {
	getKey := func(obj *model.KubernetesObject) string {
		return fmt.Sprintf("%s:%s:%s", obj.Kind, obj.Namespace, obj.Name)
	}

	// Make a map for fast searching the object.
	newResourcesMap := make(map[string]*model.KubernetesObject)
	for _, object := range request.Kubernetes {
		newResourcesMap[getKey(object)] = object
	}

	deleteResources := &request.Deleted
	deleteResources.Items = []*model.KubernetesObject{}
	for _, object := range oldApp.Kubernetes {
		if object == nil {
			continue
		}
		_, exist := newResourcesMap[getKey(object)]
		if !exist {
			deleteResources.Items = append(deleteResources.Items, object)
		}
	}

	s.Logger.Infof("Update app crd, delete resources: %s", deleteResources)

	result, err := s.Request(common.HttpPut, nil)
	if err != nil {
		return nil, err
	}

	s.Logger.Infof("Update app crd done")
	return result, nil
}

func (s *KubernetesResourceStore) GetAppSubResources(app *v1alpha1.Application) (objects []*model.KubernetesObject, err error) {
	selector := model.GetAppLabelSelector(app)

	for _, ck := range app.Spec.ComponentGroupKinds {
		resource, err := s.ResourceTypeForGVK(schema.GroupVersionKind{
			Group: ck.Group,
			Kind:  ck.Kind,
		})
		if err != nil {
			return nil, err
		}

		namespace := s.UniqueName.Namespace
		if _, exist := common.ClusterScopeResourceKind[ck.Kind]; exist {
			namespace = ""
		}

		krs := GetKubernetesResourceStore(&model.Query{
			ClusterUUID: s.UniqueName.ClusterUUID,
			Name:        "",
			Type:        resource,
			Namespace:   namespace,
		}, s.Logger, nil)
		err = krs.FetchResourceList(map[string]string{
			"labelSelector": selector,
		})
		if err != nil {
			return nil, err
		}
		for _, item := range krs.Objects.Items {
			if item.Kind == "" {
				item.Kind = ck.Kind
			}
			if item.APIVersion == "" {
				item.APIVersion = krs.Objects.ApiVersion
			}

		}
		objects = append(objects, krs.Objects.Items...)
	}
	return
}

// DeleteApplication deletes application resources first, and mark the app as deleted.
// The application will be deleted after all app resources are deleted successfully.
func (s *KubernetesResourceStore) DeleteApplication(app *model.Application) error {
	appCrd := s.Object.ToAppCrd()
	model.UpdateAnnotations(appCrd, common.AppDeletedAtKey(), time.Now().String())
	appCrd.Spec.AssemblyPhase = v1alpha1.Pending
	s.Object = model.AppCrdToObject(appCrd)
	_, err := s.Request(common.HttpPut, nil)
	if err != nil {
		return err
	}

	for _, obj := range app.Kubernetes {
		resource, err := s.ResourceTypeForObject(obj)
		if err != nil {
			return err
		}
		krs := GetKubernetesResourceStore(&model.Query{
			ClusterUUID: s.UniqueName.ClusterUUID,
			Name:        obj.Name,
			Namespace:   obj.Namespace,
			Type:        resource,
		}, s.Logger, nil)
		_, err = krs.Request(common.HttpDelete, nil)
		if err != nil {
			s.Logger.Errorf("Delete app resource %s:%s error: %s", obj.Namespace, obj.Name, err.Error())
			return err
		}
		s.Logger.Infof("Delete app resource %s:%s done", obj.Namespace, obj.Name)
	}

	// If no resources found in app, delete it directly.
	if len(app.Kubernetes) <= 0 {
		s.Object = nil
		_, err := s.Request(common.HttpDelete, nil)
		if err != nil {
			return err
		}
	}

	s.Logger.Infof("Delete app crd done")
	return nil
}

func (s *KubernetesResourceStore) ImportResourcesToApplication(request *model.ApplicationImportResourcesRequest, app *model.Application) (*rest.Result, error) {
	uuid := app.Crd.Labels[common.AppUidKey()]

	for _, obj := range request.Kubernetes {
		resource, err := s.ResourceTypeForObject(obj)
		if err != nil {
			return nil, err
		}

		namespace := app.Crd.Namespace
		if _, exist := common.ClusterScopeResourceKind[obj.Kind]; exist {
			namespace = ""
		}

		krs := GetKubernetesResourceStore(&model.Query{
			ClusterUUID: s.UniqueName.ClusterUUID,
			Name:        obj.Name,
			Namespace:   namespace,
			Type:        resource,
			PatchType:   string(types.MergePatchType),
		}, s.Logger, nil)

		if common.IsPodController(obj.Kind) {
			krs.FetchResource()
			krs.Object.SetLabel(common.AppNameKey(), app.Crd.Name)
			krs.Object.SetLabel(common.AppUidKey(), uuid)
			krs.Object.AddPodTemplateLabels(map[string]string{
				common.AppNameKey(): app.Crd.Name,
			})
			_, err = krs.Request(common.HttpPut, nil)
		} else {
			err = krs.AddResourceLabels(map[string]string{
				common.AppNameKey(): app.Crd.Name,
				common.AppUidKey():  uuid,
			})
		}

		if err != nil {
			return nil, err
		}
	}
	return s.Request(common.HttpPut, nil)
}

func (s *KubernetesResourceStore) ExportResourcesFromApplication(request *model.ApplicationExportResourcesRequest, app *model.Application) (*rest.Result, error) {
	uuid := app.Crd.Labels[common.AppUidKey()]

	for _, obj := range request.Kubernetes {
		resource, err := s.ResourceTypeForObject(obj)
		if err != nil {
			return nil, err
		}

		namespace := app.Crd.Namespace
		if _, exist := common.ClusterScopeResourceKind[obj.Kind]; exist {
			namespace = ""
		}

		krs := GetKubernetesResourceStore(&model.Query{
			ClusterUUID: s.UniqueName.ClusterUUID,
			Name:        obj.Name,
			Namespace:   namespace,
			Type:        resource,
		}, s.Logger, nil)

		if common.IsPodController(obj.Kind) {
			krs.FetchResource()
			krs.Object.RemoveLabel(common.AppNameKey())
			krs.Object.RemoveLabel(common.AppUidKey())
			krs.Object.RemovePodTemplateLabels([]string{common.AppNameKey()})
			_, err = krs.Request(common.HttpPut, nil)
		} else {
			err = krs.RemoveResourceLabels(map[string]string{
				common.AppNameKey(): app.Crd.Name,
				common.AppUidKey():  uuid,
			})
		}

		if err != nil {
			return nil, err
		}
	}
	return s.Request(common.HttpPut, nil)
}

func (s *KubernetesResourceStore) AddResourceLabels(labels map[string]string) error {
	jsonObj := gabs.New()
	jsonObj.SetP(labels, "metadata.labels")
	_, err := s.SetRaw(jsonObj.Bytes()).Request(common.HttpPatch, nil)
	return err
}

func (s *KubernetesResourceStore) RemoveResourceLabels(labels map[string]string) error {
	if err := s.FetchResource(); err != nil {
		return err
	}
	if s.Object.Labels != nil {
		for k, _ := range labels {
			delete(s.Object.Labels, k)
		}
		_, err := s.Request(common.HttpPut, nil)
		return err
	}
	return nil
}

func (s *KubernetesResourceStore) ListApplication() ([]*model.ApplicationResponse, error) {
	err := s.FetchResourceList(nil)
	if err != nil {
		return nil, err
	}

	var appList []*model.ApplicationResponse
	for _, obj := range s.Objects.Items {
		app, err := obj.ToApplication()
		if err != nil {
			return nil, err
		}
		appResp := model.ApplicationResponse{}
		appResp.Kubernetes = []*model.KubernetesObject{model.AppCrdToObject(app)}
		resources, err := s.GetAppSubResources(app)
		if err != nil {
			return nil, err
		}
		appResp.Kubernetes = append(appResp.Kubernetes, resources...)
		appList = append(appList, &appResp)
	}

	return appList, nil
}

// PatchApplication will update application crd by PATCH method.
// requirements: application.Crd set
func PatchApplication(application *model.Application, raw []byte) error {
	query := model.Query{
		ClusterUUID: application.GetClusterUUID(),
		Namespace:   application.GetNamespace(),
		Type:        "applications",
		Name:        application.Crd.Name,
		PatchType:   "application/merge-patch+json",
	}

	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &query,
			Logger:     common.GetLoggerByKV("app", application.GetUUID()),
			Raw:        raw,
		},
	}
	_, err := s.Request("PATCH", nil)
	return err

}
