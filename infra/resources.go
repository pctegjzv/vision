package infra

import (
	"encoding/json"
	"fmt"
	"time"

	"vision/common"
	"vision/config"
	"vision/model"

	"github.com/juju/errors"
	"github.com/levigross/grequests"
	log "github.com/sirupsen/logrus"
)

// DeleteResourceFromJakiro delete a a resource record from jakiro by it's uuid
func DeleteResourceFromJakiro(uuid string) error {
	resp, err := doRequest(uuid, "DELETE", nil)
	if err != nil {
		return nil
	}

	if resp.Ok {
		return nil
	} else if resp.StatusCode == 404 {
		log.Infof("Resource %v has been deleted already.", uuid)
		return nil
	} else {
		message := fmt.Sprintf("Resource %v delete error: %v, %v", uuid, resp.StatusCode, resp.String())
		log.Errorf(message)
		return errors.New(message)
	}
}

func GetResourceFromJakiro(uuid string) (*model.AlaudaInternalResource, error) {
	return getResourceFromJakiro(uuid)
}

func GetResourceFromJakiroByName(account, resourceType, name string) (*model.AlaudaInternalResource, error) {
	path := fmt.Sprintf("%s/%s/%s", account, resourceType, name)
	return getResourceFromJakiro(path)
}

func getResourceFromJakiro(path string) (*model.AlaudaInternalResource, error) {
	resp, err := doRequest(path, "GET", nil)
	if err != nil {
		return nil, err
	}
	if resp.Ok {
		var resource model.AlaudaInternalResource
		if err := json.Unmarshal(resp.Bytes(), &resource); err != nil {
			return nil, err
		} else {
			return &resource, nil
		}
	} else {
		if resp.StatusCode == 404 {
			return nil, errors.Annotate(common.ErrResourceNotExist, resp.String())
		} else {
			return nil, errors.New(resp.String())
		}
	}
}

// doRequest actually request jakiro
func doRequest(path string, method string, body interface{}) (*grequests.Response, error) {
	cfg := config.GlobalConfig.Jakiro
	if cfg.Endpoint == "" {
		log.Errorf("Jakiro inner api endpoint is empty, please set.")
		return nil, fmt.Errorf("jakiro inner api endpoint is empty, please set")
	}

	pathPrefix := fmt.Sprintf("%s/%s/inner/resources", cfg.Endpoint, cfg.ApiVersion)
	url := fmt.Sprintf("%s/%s", pathPrefix, path)

	var resp *grequests.Response
	var err error
	options := grequests.RequestOptions{RequestTimeout: time.Duration(cfg.Timeout) * time.Second}

	if method == "GET" {
		resp, err = grequests.Get(url, &options)
	} else if method == "POST" {
		options.JSON = body
		resp, err = grequests.Post(url, &options)
	} else if method == "DELETE" {
		resp, err = grequests.Delete(url, &options)
	} else {
		return nil, fmt.Errorf("unsupported http method for jakiro api")
	}

	log.Infof("request jakiro %s got code: %d data: %s %v", url, resp.StatusCode, resp.String(), err)

	return resp, err
}

func CreateJakiroResource(resource *model.AlaudaInternalResource) error {
	resp, err := doRequest("", "POST", resource)

	if err != nil {
		return err
	}
	if !resp.Ok {
		message := fmt.Sprintf("Create jakiro resource %+v error: %v, %v.", resource, resp.StatusCode, resp.String())
		return errors.New(message)
	}
	return nil
}
