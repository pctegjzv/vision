package infra

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"vision/common"
	"vision/config"
	"vision/infra/kubernetes"
)

type KubernetesConfig struct {
	Endpoint string
	Token    string
}

type KubeClient struct {
	*kubernetes.KubeResourceClient
	RedisClient *RedisClient
}

// NewKubeClient generate a new KubeClient based on regionID. It need to  retrieve kubernetes cluster
// info from cache or furion.
// also will  add region to watch list
func NewKubeClient(regionID string) (*KubeClient, error) {
	if regionID == "" {
		return nil, errors.New("region uuid is empty when generate kube client")
	}
	cfg, err := GetKubernetesConfig(regionID)
	if err != nil {
		return nil, err
	}

	redis, err := GetRedis()
	if err != nil {
		log.Errorf("Get redis error: %s", err.Error())
	}

	cacheConfig := kubernetes.RedisCacheConfig{
		RedisCache: redis,
		Expiration: common.GetTimeDuration(config.GlobalConfig.Kubernetes.ApiDiscoveryExpiration),
	}
	client, err := kubernetes.NewKubeResourceClient(regionID, cfg.Endpoint, cfg.Token, cacheConfig)
	if err != nil {
		return nil, err
	}

	return &KubeClient{
		KubeResourceClient: client,
		RedisClient:        redis,
	}, nil
}
