package infra

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"vision/common"
	"vision/config"

	"github.com/alauda/bergamot/diagnose"
	"github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
	"github.com/juju/errors"
)

var (
	GlobalRedis RedisClient

	ErrObtainLockFailed = errors.New("obtain lock failed")
)

func (r *RedisClient) k(key string) string {
	return r.prefix + key
}

func (r *RedisClient) ks(keys []string) []string {
	var result []string
	for _, key := range keys {
		result = append(result, r.k(key))
	}
	return result
}

func (r *RedisClient) e(err error) error {
	if err == redis.Nil {
		return common.ErrNotFound
	}
	return err
}

func (r *RedisClient) ne(err error) error {
	if err == redis.Nil {
		return nil
	}
	return err
}

// Get gets a value from a key
func (r *RedisClient) Get(key string) (string, error) {
	val, err := r.read.Get(r.k(key)).Result()
	return val, r.e(err)
}

// Set sets a value to a key
func (r *RedisClient) Set(key string, value interface{}, expire time.Duration) error {
	return r.write.Set(r.k(key), value, expire).Err()
}

func (r *RedisClient) Persist(key string) error {
	return r.write.Persist(r.k(key)).Err()
}

func (r *RedisClient) Exists(key string) (bool, error) {
	result, err := r.read.Exists(r.k(key)).Result()
	return result == 1, r.ne(err)
}

// HGet will get a field value from a hash key.
func (r *RedisClient) HGet(key string, field string) (string, error) {
	val, err := r.read.HGet(r.k(key), field).Result()
	return val, r.e(err)
}

func (r *RedisClient) HDel(key string, field string) (int64, error) {
	val, err := r.write.HDel(r.k(key), field).Result()
	return val, r.ne(err)
}

func (r *RedisClient) HKeys(key string) ([]string, error) {
	ss, err := r.read.HKeys(r.k(key)).Result()
	return ss, r.e(err)
}

func (r *RedisClient) HExists(key string, field string) (bool, error) {
	result, err := r.read.HExists(r.k(key), field).Result()
	return result, r.e(err)
}

// HSet will set a field value to a hash key.
func (r *RedisClient) HSet(key string, field string, value interface{}) error {
	return r.write.HSet(r.k(key), field, value).Err()
}

// Del deletes given number of keys
func (r *RedisClient) Del(keys ...string) (int64, error) {
	return r.write.Del(r.ks(keys)...).Result()
}

func (r *RedisClient) NoPrefixDel(keys ...string) (int64, error) {
	return r.write.Del(keys...).Result()
}

// Expire adds an expiration date to a key
func (r *RedisClient) Expire(key string, expiration time.Duration) (bool, error) {
	return r.write.Expire(r.k(key), expiration).Result()
}

// ScanAll scans for all matched keys. (without prefix)
func (r *RedisClient) ScanAll(match string) ([]string, error) {
	return r.ScanAllForEachMaster(match)
}

func (r *RedisClient) ScanAllForEachMaster(match string) ([]string, error) {
	var keys []string
	lock := &sync.Mutex{}
	match = r.k(match)
	scanFunc := func(client redis.UniversalClient) ([]string, error) {
		var mKeys []string
		ks, cursor, err := client.Scan(0, match, 1000).Result()
		for cursor != 0 {
			mKeys = append(mKeys, ks...)
			ks, cursor, err = client.Scan(cursor, match, 1000).Result()
			if err != nil {
				return nil, err
			}
		}
		if len(ks) > 0 {
			mKeys = append(mKeys, ks...)
		}
		return mKeys, err
	}

	var err error
	cc, ok := r.read.(*redis.ClusterClient)
	if ok {
		err = cc.ForEachMaster(func(client *redis.Client) error {
			mKeys, err := scanFunc(client)
			if err != nil {
				return err
			}
			lock.Lock()
			defer lock.Unlock()
			keys = append(keys, mKeys...)
			return nil
		})
	} else {
		keys, err = scanFunc(r.read)
	}

	if err != nil {
		return nil, err
	}

	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	return keys, nil
}

func (r *RedisClient) IsReadCluster() bool {
	_, ok := r.read.(*redis.ClusterClient)
	return ok
}

// MGet gets multiple values from keys. (with prefix)
// ISSUE: https://github.com/go-redis/redis/issues/291
func (r *RedisClient) MGet(keys ...string) ([]string, error) {

	var res []string

	if r.IsReadCluster() {
		pipe := r.read.Pipeline()
		for _, k := range keys {
			if err := pipe.Process(r.read.Get(k)); err != nil {
				return nil, err
			}
		}
		cmders, err := pipe.Exec()
		if err != nil {
			return nil, err
		}
		for _, cmder := range cmders {
			result, _ := cmder.(*redis.StringCmd).Result()
			res = append(res, result)
		}
		return res, nil
	}

	vals, err := r.read.MGet(keys...).Result()

	if redis.Nil != err && nil != err {
		return nil, err
	}

	for _, item := range vals {
		res = append(res, fmt.Sprintf("%s", item))
	}
	return res, err
}

func (r *RedisClient) GetLocker(key string) (*lock.Locker, error) {
	timeout := common.GetTimeDuration(config.GlobalConfig.Vision.DeployTimeout)
	locker, err := lock.Obtain(r.write, key, &lock.Options{LockTimeout: timeout})
	if locker == nil {
		err = ErrObtainLockFailed
	}
	return locker, err
}

// Diagnose start diagnose check
// http://confluence.alaudatech.com/pages/viewpage.action?pageId=14123161
func (r *RedisClient) Diagnose() diagnose.ComponentReport {
	var (
		err   error
		start time.Time
	)

	report := diagnose.NewReport("redis")
	start = time.Now()
	err = r.read.Ping().Err()
	report.AddLatency(start)
	report.Check(err, "Redis reader ping failed", "Check environment variables or redis health")
	start = time.Now()
	err = r.write.Ping().Err()
	report.AddLatency(start)
	report.Check(err, "Redis writer ping failed", "Check environment variables or redis health")
	return *report
}

// RedisClient support
// 1. read-write separate
// 2. key prefix
// 3. cluster/single node mode
type RedisClient struct {
	read   redis.UniversalClient
	write  redis.UniversalClient
	prefix string
}

func getAddrList(hosts, ports []string) []string {
	addrs := make([]string, len(hosts))
	for idx, host := range hosts {
		port := ports[idx]
		addrs = append(addrs, fmt.Sprintf("%s:%s", host, port))
	}
	addrs = common.RemoveEmptyString(addrs)
	return addrs

}

// TODO: ping check
func GetRedis() (*RedisClient, error) {

	if GlobalRedis.read != nil && GlobalRedis.write != nil {
		return &GlobalRedis, nil
	}

	cfg := config.GlobalConfig.Redis
	timeout := common.GetTimeDuration(3)
	readOptions := redis.UniversalOptions{
		Addrs:       getAddrList(cfg.HostReader, cfg.PortReader),
		DB:          cfg.DBNameReader,
		Password:    cfg.DBPasswordReader,
		PoolSize:    cfg.MaxConnectionsReader,
		DialTimeout: timeout,
		ReadTimeout: timeout,
		ReadOnly:    true,
	}
	writeOptions := redis.UniversalOptions{
		Addrs:        getAddrList(cfg.HostWriter, cfg.PortWriter),
		DB:           cfg.DBNameWriter,
		Password:     cfg.DBPasswordWriter,
		PoolSize:     cfg.MaxConnectionsWriter,
		DialTimeout:  timeout,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}

	client := RedisClient{
		read:   redis.NewUniversalClient(&readOptions),
		write:  redis.NewUniversalClient(&writeOptions),
		prefix: cfg.KeyPrefixReader,
	}

	if _, err := client.read.Ping().Result(); err != nil {
		return nil, err
	} else {
		GlobalRedis = client
		return &client, err
	}
}
