package kubernetes

import (
	"errors"

	"k8s.io/apimachinery/pkg/api/meta"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/rest"
)

// CachedRESTMapper can cache the discovery result for REST mapping of resources.
type CachedRESTMapper struct {
	cfg             *rest.Config
	mapper          *discovery.DeferredDiscoveryRESTMapper
	cachedDiscovery discovery.CachedDiscoveryInterface
}

var (
	// ErrorResourceKindNotFound is an error that a kind for resource could not be found.
	ErrorResourceKindNotFound = errors.New("resource kind not found")
)

// NewCachedRESTMapper creates a CachedRESTMapper object.
func NewCachedRESTMapper(cfg rest.Config, cacheConfig RedisCacheConfig) (*CachedRESTMapper, error) {
	dc, err := discovery.NewDiscoveryClientForConfig(&cfg)
	if err != nil {
		return nil, err
	}

	cachedDiscovery := NewRedisCachedClient(dc, cacheConfig)
	mapper := discovery.NewDeferredDiscoveryRESTMapper(cachedDiscovery, meta.InterfacesForUnstructured)

	return &CachedRESTMapper{
		cfg:             &cfg,
		mapper:          mapper,
		cachedDiscovery: cachedDiscovery,
	}, nil
}

// Reset resets the cached discovery result.
func (c *CachedRESTMapper) Reset() {
	c.mapper.Reset()
}

// ConfigForResource returns a rest config for a resource type.
func (c *CachedRESTMapper) ConfigForResource(gvr schema.GroupVersionResource) (rest.Config, error) {
	c.cfg.GroupVersion = nil
	gvk, err := c.mapper.KindFor(gvr)
	if err != nil {
		if meta.IsNoMatchError(err) {
			err = ErrorResourceKindNotFound
		}
		return rest.Config{}, err
	}

	mapping, err := c.mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
	if err != nil {
		return rest.Config{}, err
	}

	gv := mapping.GroupVersionKind.GroupVersion()
	c.cfg.GroupVersion = &gv
	typer := unstructuredObjectTyper{
		unstructuredTyper: discovery.NewUnstructuredObjectTyper(nil),
	}
	creator := unstructuredCreator{}

	c.cfg.NegotiatedSerializer = unstructuredNegotiatedSerializer{typer: typer, creator: creator}
	switch gvk.Group {
	case "":
		c.cfg.APIPath = "/api"
	default:
		c.cfg.APIPath = "/apis"
	}
	return *c.cfg, nil
}

// ResourceNameForGVK returns a resource name for a gvk.
func (c *CachedRESTMapper) ResourceNameForGVK(gvk schema.GroupVersionKind) (string, error) {
	mapping, err := c.mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
	if err != nil {
		return "", err
	}

	return mapping.Resource, nil
}

func (c *CachedRESTMapper) ServerPreferredResources() ([]*metaV1.APIResourceList, error) {
	return c.cachedDiscovery.ServerPreferredResources()
}
