package kubernetes

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"vision/config"

	"github.com/googleapis/gnostic/OpenAPIv2"
	log "github.com/sirupsen/logrus"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/discovery"
	restclient "k8s.io/client-go/rest"
)

// RedisCacheConfig describes a config to create a redis cache client.
type RedisCacheConfig struct {
	RedisCache RedisCacheInterface
	Expiration time.Duration
	keyPrefix  string
}

// RedisCacheInterface defines an interface to access a redis cache.
type RedisCacheInterface interface {
	Get(key string) (string, error)
	Exists(key string) (bool, error)
	Set(key string, value interface{}, expiration time.Duration) error
	Persist(key string) error
	Del(key ...string) (int64, error)
	Expire(key string, expiration time.Duration) (bool, error)
}

// redisCachedClient defines a struct implements CachedDiscoveryInterface to use redis cache.
type redisCachedClient struct {
	delegate discovery.DiscoveryInterface

	sync.RWMutex
	resourcesKeyPrefix string
	groupsKey          string
	*RedisCacheConfig
}

var (
	// ErrCacheNotFound is an error that a record is not found in redis cache.
	ErrCacheNotFound = errors.New("not found in redis cache")
)

// Ensure redisCachedClient implement CachedDiscoveryInterface
var _ discovery.CachedDiscoveryInterface = &redisCachedClient{}

func isBlackGroup(group string) bool {
	bgrps := config.GlobalConfig.Kubernetes.DiscoveryBlackGroups
	for _, gb := range bgrps {
		if strings.Contains(group, gb) {
			return true
		}
	}
	return false
}

// ServerResourcesForGroupVersion returns the supported resources for a group and version.
// It first queries from the redis, if not found, updates the data from the delegate and
// update the redis cache.
func (r *redisCachedClient) ServerResourcesForGroupVersion(groupVersion string) (*metaV1.APIResourceList, error) {
	r.RLock()
	defer r.RUnlock()
	resources, err := r.getResourcesFromRedis(groupVersion)
	// If cache error, try to update the cache with the latest data,
	// use inline function to avoid dead lock.
	if err == ErrCacheNotFound {
		func() {
			r.RUnlock()
			r.Lock()
			defer func() {
				r.Unlock()
				r.RLock()
			}()
			log.Debugf("Try to update cache for %s", r.getResourcesRedisKey(groupVersion))
			resources, err = r.updateServerResourcesForGroupVersion(groupVersion)
		}()
	}

	if err != nil {
		return nil, err
	}

	return resources, nil
}

// updateServerResourcesForGroupVersion updates resources of the group version to the redis
// cache from the delegate.
func (r *redisCachedClient) updateServerResourcesForGroupVersion(groupVersion string) (*metaV1.APIResourceList, error) {
	resources, err := r.delegate.ServerResourcesForGroupVersion(groupVersion)
	if err != nil {
		log.Errorf("Error get server resources for group version %s: %v", groupVersion, err)
		return nil, err
	}

	if err = r.setResourcesToRedis(groupVersion, resources); err != nil {
		log.Errorf("Set resources error: %v", err)
	}
	return resources, nil
}

// ServerResources returns the supported resources for all groups and versions.
func (r *redisCachedClient) ServerResources() ([]*metaV1.APIResourceList, error) {
	apiGroups, err := r.ServerGroups()
	if err != nil {
		return nil, err
	}
	groupVersions := metaV1.ExtractGroupVersions(apiGroups)
	var result []*metaV1.APIResourceList
	for _, groupVersion := range groupVersions {
		resources, err := r.ServerResourcesForGroupVersion(groupVersion)
		if err != nil {
			return nil, err
		}
		result = append(result, resources)
	}
	return result, nil
}

// ServerGroups returns all groups. It first queries from the redis, if not
// found, updates the data from the delegate and update the redis cache.
func (r *redisCachedClient) ServerGroups() (*metaV1.APIGroupList, error) {
	r.RLock()
	defer r.RUnlock()
	gl, err := r.getGroupsFromRedis()

	// If cache error, try to update the cache with the latest data
	// use inline function to avoid dead lock.
	if err == ErrCacheNotFound {
		func() {
			r.RUnlock()
			r.Lock()
			defer func() {
				r.Unlock()
				r.RLock()
			}()
			gl, err = r.updateServerGroups()
		}()
	}

	if err != nil {
		return nil, err
	}

	return gl, nil
}

// updateServerGroups updates groups to the redis cache from the delegate.
func (r *redisCachedClient) updateServerGroups() (*metaV1.APIGroupList, error) {
	gl, err := r.delegate.ServerGroups()
	if err != nil || len(gl.Groups) == 0 {
		log.Errorf("couldn't get current server API group list; will keep using cached value. (%v)", err)
		return nil, err
	}

	var newGrps []metaV1.APIGroup
	for _, g := range gl.Groups {
		if !isBlackGroup(g.Name) {
			newGrps = append(newGrps, g)
		}
	}
	gl.Groups = newGrps

	if err = r.setGroupsToRedis(gl); err != nil {
		log.Errorf("Set groups error: %v", err)
	}
	return gl, nil
}

// RESTClient returns the REST client.
func (r *redisCachedClient) RESTClient() restclient.Interface {
	return r.delegate.RESTClient()
}

// ServerPreferredResources returns the servers preferred resources.
func (r *redisCachedClient) ServerPreferredResources() ([]*metaV1.APIResourceList, error) {
	return r.delegate.ServerPreferredResources()
}

// ServerPreferredNamespacedResources returns the servers preferred namespaced resources.
func (r *redisCachedClient) ServerPreferredNamespacedResources() ([]*metaV1.APIResourceList, error) {
	return r.delegate.ServerPreferredNamespacedResources()
}

// ServerVersion returns the server version.
func (r *redisCachedClient) ServerVersion() (*version.Info, error) {
	return r.delegate.ServerVersion()
}

// OpenAPISchema returns the document of the Open API Schema.
func (r *redisCachedClient) OpenAPISchema() (*openapi_v2.Document, error) {
	return r.delegate.OpenAPISchema()
}

// Fresh returns whether the cache is fresh.
func (r *redisCachedClient) Fresh() bool {
	// Always return true because the cache will expired and could keep update.
	return true
}

// Invalidate refreshes the cache, blocking calls until the cache has been
// refreshed. It would be trivial to make a version that does this in the
// background while continuing to respond to requests if needed.
func (r *redisCachedClient) Invalidate() {
	r.Lock()
	defer r.Unlock()

	gl, err := r.delegate.ServerGroups()
	if err != nil || len(gl.Groups) == 0 {
		log.Errorf("couldn't get current server API group list; will keep using cached value. (%v)", err)
		return
	}

	for _, g := range gl.Groups {
		for _, v := range g.Versions {
			res, err := r.delegate.ServerResourcesForGroupVersion(v.GroupVersion)
			if err != nil || len(res.APIResources) == 0 {
				log.Errorf("couldn't get resource list for %v: %v", v.GroupVersion, err)
				continue
			}
			if err := r.setResourcesToRedis(v.GroupVersion, res); err != nil {
				log.Errorf("set resources to redis error: %v", err)
			}
		}
	}
	if err := r.setGroupsToRedis(gl); err != nil {
		log.Errorf("set groups to redis error: %v", err)
	}
}

func (r *redisCachedClient) isRedisAvailable() bool {
	return !reflect.ValueOf(r.RedisCache).IsNil()
}

// setResourcesToRedis sets the resources to redis.
func (r *redisCachedClient) setResourcesToRedis(groupVersion string, resources *metaV1.APIResourceList) error {
	if !r.isRedisAvailable() {
		return errors.New("redis is not available")
	}

	var err error
	if jsonRes, err := json.Marshal(resources); err == nil {
		log.Debugf("Set cache for gv %s with expiration %v", groupVersion, r.Expiration)
		return r.RedisCache.Set(r.getResourcesRedisKey(groupVersion), string(jsonRes), r.Expiration)
	}
	return err
}

// getResourcesFromRedis gets the resources from redis.
func (r *redisCachedClient) getResourcesFromRedis(groupVersion string) (*metaV1.APIResourceList, error) {
	if !r.isRedisAvailable() {
		return nil, errors.New("redis is not available")
	}
	if exist, _ := r.RedisCache.Exists(r.getResourcesRedisKey(groupVersion)); !exist {
		log.Debugf("resource %s not exist in redis", r.getResourcesRedisKey(groupVersion))
		return nil, ErrCacheNotFound
	}
	result, err := r.RedisCache.Get(r.getResourcesRedisKey(groupVersion))
	if err != nil {
		log.Errorf("get cache for %s error: %v", r.getResourcesRedisKey(groupVersion), err)
		return nil, err
	}

	resources := &metaV1.APIResourceList{}
	err = json.Unmarshal([]byte(result), resources)
	if err != nil {
		return nil, err
	}

	return resources, nil
}

// setGroupsToRedis sets groups to redis.
func (r *redisCachedClient) setGroupsToRedis(groups *metaV1.APIGroupList) error {
	if !r.isRedisAvailable() {
		return errors.New("redis is not available")
	}
	var err error
	if jsonGrp, err := json.Marshal(groups); err == nil {
		log.Debugf("Set cache for groups '%s' with expiration %v", r.groupsKey, r.Expiration)
		return r.RedisCache.Set(r.groupsKey, string(jsonGrp), r.Expiration)
	}
	return err
}

// getGroupsFromRedis gets groups from redis.
func (r *redisCachedClient) getGroupsFromRedis() (*metaV1.APIGroupList, error) {
	if !r.isRedisAvailable() {
		return nil, errors.New("redis is not available")
	}
	if exist, _ := r.RedisCache.Exists(r.groupsKey); !exist {
		log.Debugf("groups %s not exist in redis", r.groupsKey)
		return nil, ErrCacheNotFound
	}
	result, err := r.RedisCache.Get(r.groupsKey)
	if err != nil {
		log.Debugf("get groups from redis error: %v", err)
		return nil, err
	}

	groups := &metaV1.APIGroupList{}
	err = json.Unmarshal([]byte(result), groups)
	if err != nil {
		return nil, err
	}

	return groups, nil
}

// getResourcesRedisKey gets redis key of the resources.
func (r *redisCachedClient) getResourcesRedisKey(groupVersion string) string {
	return fmt.Sprintf("%s:%s", r.resourcesKeyPrefix, groupVersion)
}

// NewRedisCachedClient creates a redis cached client for discovery.
func NewRedisCachedClient(delegate discovery.DiscoveryInterface, config RedisCacheConfig) discovery.CachedDiscoveryInterface {
	return &redisCachedClient{
		delegate:           delegate,
		resourcesKeyPrefix: fmt.Sprintf("%s:resources", config.keyPrefix),
		groupsKey:          fmt.Sprintf("%s:groups", config.keyPrefix),
		RedisCacheConfig:   &config,
	}
}
