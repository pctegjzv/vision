package infra

import (
	"strings"

	"vision/common"
	"vision/model"

	"github.com/juju/errors"
)

// GetMatchCacheKeys fetch all keys match the patten in cache
func GetMatchCacheKeys(pattern string) ([]string, error) {
	r, err := GetRedis()
	if err != nil {
		return nil, err
	}
	return r.ScanAll(pattern)
}

// GetMatchObjects retrieve list of data from cache which keys match the pattern
func GetMatchData(pattern string) ([]string, error) {
	keys, err := GetMatchCacheKeys(pattern)
	if err != nil {
		return nil, err
	}

	r, err := GetRedis()
	if err != nil {
		return nil, err
	}

	result, err := r.MGet(keys...)
	if err != nil {
		return nil, errors.Annotate(err, "mget resource from cache error")
	}
	return result, nil
}

func (c *KubeClient) IsRegionWatcherShouldExpire(regionID string) (bool, error) {
	if c.RedisClient == nil {
		return false, errors.Errorf("Redis not working")
	}
	exist, err := c.RedisClient.HExists(common.RegionWatcherKey, regionID)
	if err != nil {
		c.Logger.WithError(err).Error("Check region watcher key error!")
		return false, err
	}
	return !exist, nil
}

// GetKubernetesResourceCacheKey get redis cache key by k8s resource uid
// format:  k8s:uid:<uid>
// TODO(xxhe): Should be removed if v3 API have migrated to use name as key.
func GetKubernetesResourceCacheKey(uid string) string {
	return common.CombineString(common.KubernetesResourceUIDCacheKeyPrefix, uid)
}

// GetKubernetesNameCacheKey get a resource name to uid mapping key
// format: k8s:name:<cluster_uuid>:<namespace>:<resource_type>:<name>
func GetKubernetesNameCacheKey(clusterUUID, name, resourceType, namespace string) string {
	return common.CombineString(
		common.KubernetesResourceNameCacheKeyPrefix,
		clusterUUID,
		namespace,
		resourceType,
		name,
	)
}

// GetKubernetesCacheKeyMatch gets a key match string of a resource type.
// Format: k8s:name:<cluster_uuid>:<namespace>:<resource_type>
// Note: if namespace is "", will be replaced with "*".
func GetKubernetesCacheKeyMatch(clusterUUID, namespace, resourceType string) string {
	if namespace == "" {
		namespace = "*"
	}
	if resourceType == "" {
		resourceType = "*"
	}
	return common.CombineString(common.KubernetesResourceNameCacheKeyPrefix,
		clusterUUID, namespace, resourceType, "*")
}

// ParseNameKeyToQuery parse a name key to Query
// format: k8s:name:<cluster_uuid>:<namespace><resource_type>:<name>
func ParseNameKeyToQuery(name string) *model.Query {
	items := strings.Split(name, ":")
	query := model.Query{
		ClusterUUID: items[2],
		Namespace:   items[3],
		Type:        items[4],
	}
	if len(items) == 6 {
		query.Name = items[5]
	} else {
		query.Name = strings.Join(items[5:], ":")
	}
	return &query
}

func ParseQueryToNameKey(query *model.Query) string {
	return GetKubernetesNameCacheKey(
		query.ClusterUUID,
		query.Name,
		query.Type,
		query.Namespace,
	)
}
