package infra

import "testing"

func TestSingleRegion(t *testing.T) {
	data := `{
      "id": "9636741d-63bb-465e-8483-af69d0d41228",
      "platform_version": "v3",
      "features": {
        "kubernetes": {
          "endpoint": "https://139.219.56.162:6443",
          "token": "7dd33a.84c08dc99025557f",
          "version": "1.7.3"
        }
      }
    }`
	result, err := getKubernetesConfig(data)
	if err != nil {
		t.Errorf("parse kubernetes config error: %s", err.Error())
	}
	if result.Token != "7dd33a.84c08dc99025557f" {
		t.Errorf("parse kubernetes token error: %s", result.Token)
	}
	if result.Endpoint != "https://139.219.56.162:6443" {
		t.Errorf("parse kubernetes endpoint error: %s", result.Endpoint)
	}
}
