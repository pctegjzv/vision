OS = Linux

VERSION = 0.0.1

CURDIR = $(shell pwd)
SOURCEDIR = $(CURDIR)
COVER = $($3)

ECHO = echo
RM = rm -rf
MKDIR = mkdir

# If the first argument is "cover"...
ifeq (cover,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (mysql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (psql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (test, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: test worker


.DEFAULT_GOAL := all


unit-test:
	 go test -v -tags unit -cover ./infra  ./tests

# run only on linux
redis-test:
	docker rm -f local-redis-test || echo "clean up"
	docker run --name=local-redis-test -d --net=host grokzen/redis-cluster
	go test -v  -cover ./tests/redis
	docker rm -f local-redis-test


setup:
	go get -u github.com/golang/dep/cmd/dep

dep:
	dep ensure -v

fmt:
	./run/scripts/check-fmt.sh

all: setup fmt unit-test bin

cover:
	./scripts/gen-cover.sh $(RUN_ARGS)

bin:
	go build -ldflags "-w -s" -v -o vision vision

build: bin unit-test

clean:
	./scripts/clean.sh

compile:
	go build -ldflags "-w -s" -v -o $(ALAUDACI_DEST_DIR)/bin/vision vision

gen-mock:
	mockgen -destination ./mock/stores.go -package mock vision/domain TemplateStore,ProjectStore
	mockgen -destination ./mock/workflow.go -package mock vision/control Workflower
	mockgen -destination ./mock/controllers.go -package mock vision/inter TemplateController,ProjectController

image:
	docker build -t vision .

mysql:
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml build --force-rm
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml up -d

psql:
	docker-compose -p vision -f run/docker/psql-compose.yaml $(RUN_ARGS)

pr:
	docker-compose -p vision -f run/docker/psql-redis-cluster.yaml build --force-rm
	docker-compose -p vision -f run/docker/psql-redis-cluster.yaml up -d

compose:
	docker-compose -p vision -f run/docker/psql-compose.yaml build --force-rm
	docker-compose -p vision -f run/docker/psql-compose.yaml up -d

compose-linux:
	go build -ldflags "-w -s" -v -o vision vision
	docker-compose -p vision -f run/docker/psql-compose.linux.yaml build --force-rm
	docker-compose -p vision -f run/docker/psql-compose.linux.yaml up -d


cloc:
	cloc . --exclude-dir=vendor,.idea

install:
	go install

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "image             - build docker image"
	@$(ECHO) "compose           - docker compose to build and start this project"
	@$(ECHO) "up                - docker compose start service"
	@$(ECHO) "setup             - get dep tool"
	@$(ECHO) "cloc              - cal code lines"
	@$(ECHO) "dep               - make dependencies(vendor) update to date (need http proxy)"
	
