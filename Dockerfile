FROM golang:1.8


COPY . $GOPATH/src/vision


WORKDIR $GOPATH/src/vision

ENV KROBELUS=$GOPATH/src/vision

RUN go install && chmod +x run/scripts/*.sh && mkdir -p /var/log/mathilde


EXPOSE 8080 6070

CMD ["vision"]
