package config

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Vision struct {
		Debug         bool   `envconfig:"default=true"`
		DeployTimeout int    `envconfig:"default=180"`
		DeployMode    string `envconfig:"default=release"`
		Port          int    `envconfig:"default=6060"`
	}

	Platform struct {
		DefaultProject string `envconfig:"default=default"`
	}

	Log struct {
		// unit: M
		Size   int `envconfig:"default=100"`
		Backup int `envconfig:"default=2"`
	}

	Jakiro struct {
		Endpoint   string `envconfig:"optional"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://mock-server:80"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Kubernetes struct {
		Timeout                int      `envconfig:"default=3"`
		ApiDiscoveryExpiration int      `envconfig:"default=300"`
		DiscoveryBlackGroups   []string `envconfig:"optional"`
	}

	Label struct {
		BaseDomain string `envconfig:"default=alauda.io"`
	}

	Watcher struct {
		// default to 12hours.
		Timeout int `envconfig:"default=43200"`
		// resync every half hour.
		ResyncPeriod int `envconfig:"default=1800, WATCHER_RESYNC_PERIOD"`
	}

	DB struct {
		Host        string `envconfig:"default=127.0.0.1"`
		Port        string `envconfig:"default=5432"`
		User        string `envconfig:"default=vision"`
		Password    string `envconfig:"default=123456"`
		Name        string `envconfig:"default=visiondb"`
		Engine      string `envconfig:"default=postgresql"`
		Timeout     int    `envconfig:"default=0"`
		MaxConn     int    `envconfig:"default=10"`
		MaxIdleConn int    `envconfig:"default=1, DB_MAX_IDLE_CONN"`

		TimeZoneDiff int `envconfig:"default=0"`
	}

	Redis struct {
		//   Prefix string `envconfig:"default=vision:"`

		TypeWriter           string `envconfig:"default=normal"`
		TypeReader           string `envconfig:"default=normal"`
		HostWriter           []string
		HostReader           []string
		PortReader           []string
		PortWriter           []string
		DBNameWriter         int    `envconfig:"default=0,REDIS_DB_NAME_WRITER"`
		DBNameReader         int    `envconfig:"default=0,REDIS_DB_NAME_READER"`
		DBPasswordReader     string `envconfig:"optional"`
		DBPasswordWriter     string `envconfig:"optional"`
		MaxConnectionsReader int    `envconfig:"default=32"`
		MaxConnectionsWriter int    `envconfig:"default=32"`
		// KeyPrefix should keep the same, use Reader if not
		KeyPrefixWriter string `envconfig:"default=krobelus:"`
		KeyPrefixReader string `envconfig:"default=krobelus:"`
	}
}

func InitConfig() {
	err := GlobalConfig.LoadFromEnv()
	if err != nil {
		log.Errorf("Load config from env error : %s", err.Error())
		os.Exit(1)
	}
}

// LoadFromEnv load env to the Config struct, also check redis env host/port match.
func (conf *Config) LoadFromEnv() error {
	if err := envconfig.Init(conf); err != nil {
		return err
	}

	if conf.Redis.KeyPrefixReader != conf.Redis.KeyPrefixWriter {
		conf.Redis.KeyPrefixWriter = conf.Redis.KeyPrefixReader
	}

	return nil
}
