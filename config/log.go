package config

import (
	"os"

	"github.com/natefinch/lumberjack"
	log "github.com/sirupsen/logrus"
	"github.com/yayua/kvlog"
)

func SetLogger() {
	formatter := kvlog.New(kvlog.WithPrimaryFields("request_id", "action"), kvlog.IncludeCaller())
	log.SetFormatter(formatter)
	if GlobalConfig.Vision.Debug {
		log.SetOutput(os.Stderr)
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
		log.SetOutput(&lumberjack.Logger{
			Filename:   "/var/log/mathilde/vision.log",
			MaxSize:    GlobalConfig.Log.Size, // megabytes
			MaxBackups: GlobalConfig.Log.Backup,
			MaxAge:     28,    //days
			Compress:   false, // disabled by default
		})
	}

}
