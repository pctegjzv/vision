package tests

import (
	"testing"
	"time"

	"vision/common"
	"vision/config"
	"vision/infra"
	"vision/tests"
)

func setup() {
	config.GlobalConfig.Redis.HostReader = []string{"127.0.0.1", "127.0.0.1", "127.0.0.1"}
	config.GlobalConfig.Redis.HostWriter = config.GlobalConfig.Redis.HostReader
	config.GlobalConfig.Redis.PortReader = []string{"7000", "7001", "7002"}
	config.GlobalConfig.Redis.PortWriter = config.GlobalConfig.Redis.PortReader
	config.GlobalConfig.Redis.KeyPrefixReader = "kro:"
	config.GlobalConfig.Redis.KeyPrefixWriter = config.GlobalConfig.Redis.KeyPrefixReader
}

func TestGetClusterRedis(t *testing.T) {
	setup()
	redis, err := infra.GetRedis()
	if err != nil {
		t.Errorf("Init redis error: %s", err.Error())
	}
	if redis == nil {
		t.Error("redis client is nil")
	}
}

func TestClusterSetGet(t *testing.T) {
	redis, _ := infra.GetRedis()
	err := redis.Set("a", "b", common.GetTimeDuration(2))
	tests.OK(t, err)
	t.Log("set pass")

	result, err := redis.Get("a")
	tests.OK(t, err)
	tests.EQ(t, result, "b")
	t.Log("get pass")

	exist, err := redis.Exists("a")
	tests.OK(t, err)
	tests.EQ(t, exist, true)
	t.Log("exists pass")

	time.Sleep(common.GetTimeDuration(3))

	result, err = redis.Get("a")
	tests.EQ(t, result, "")
	tests.EQ(t, err, common.ErrNotFound)
	t.Log("expire get pass")

	err = redis.Set("a", "b", common.GetTimeDuration(2))
	tests.OK(t, err)
	err = redis.Persist("a")
	tests.OK(t, err)

	time.Sleep(common.GetTimeDuration(3))
	result, err = redis.Get("a")
	tests.OK(t, err)
	tests.EQ(t, result, "b")
	t.Log("persist pass")

	count, err := redis.Del("a")
	tests.EQ(t, count, int64(1))
	tests.OK(t, err)
	t.Log("del pass")

}

func TestClusterHash(t *testing.T) {
	redis, err := infra.GetRedis()
	tests.OK(t, err)

	err = redis.HSet("a", "b", "c")
	tests.OK(t, err)
	err = redis.HSet("a", "c", "b")
	tests.OK(t, err)
	t.Log("hset pass")

	result, err := redis.HGet("a", "b")
	tests.OK(t, err)
	tests.EQ(t, result, "c")
	t.Log("hget pass")

	result, err = redis.HGet("c", "a")
	tests.EQ(t, err, common.ErrNotFound)
	tests.EQ(t, result, "")
	t.Log("hget not found 1 pass")

	result, err = redis.HGet("a", "e")
	tests.EQ(t, err, common.ErrNotFound)
	tests.EQ(t, result, "")
	t.Log("hget not found 2 pass")

	keys, err := redis.HKeys("a")
	tests.OK(t, err)
	tests.EQ(t, keys, []string{"b", "c"})
	t.Log("hkeys pass")

	count, err := redis.HDel("a", "b")
	tests.OK(t, err)
	tests.EQ(t, count, int64(1))
	t.Log("hdel pass")

	exist, err := redis.HExists("a", "b")
	tests.OK(t, err)
	tests.EQ(t, false, exist)
	exist, err = redis.HExists("a", "c")
	tests.OK(t, err)
	tests.EQ(t, true, exist)
	t.Log("hexists pass")

	count, err = redis.NoPrefixDel("kro:a")
	tests.EQ(t, count, int64(1))
	tests.OK(t, err)
}

func TestClusterScanMGet(t *testing.T) {
	redis, _ := infra.GetRedis()
	expire := common.GetTimeDuration(3)
	keys := []string{"test-a", "test-b", "test-c", "test-d"}
	redis.Set(keys[0], "b", expire)
	redis.Set(keys[1], "a", expire)
	redis.Set(keys[2], "d", expire)
	redis.Set(keys[3], "f", expire)

	result, err := redis.ScanAll("test-*")
	tests.OK(t, err)
	var newKeys []string
	for _, item := range keys {
		newKeys = append(newKeys, config.GlobalConfig.Redis.KeyPrefixReader+item)
	}
	tests.EQ(t, newKeys, result)
	t.Log("scan pass")

	result, err = redis.MGet(result...)
	tests.OK(t, err)
	tests.EQ(t, []string{"b", "a", "d", "f"}, result)
	t.Log("mget pass")

}
