package tests

import (
	"testing"

	"vision/common"
)

func TestGetKubernetesTypes(t *testing.T) {
	if common.GetKubernetesTypeFromKind("Secret") != "secrets" {
		t.Errorf("Trans type secret error")
	}
	if common.GetKubernetesTypeFromKind("StorageClass") != "storageclasses" {
		t.Errorf("Trans type storageclass error")
	}
}
