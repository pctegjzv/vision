package tests

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"vision/common"
	"vision/config"
	"vision/infra"
	"vision/model"

	"github.com/juju/errors"
)

var (
	JakiroNotFoundErrorResp = []byte(`{"errors": [{"source": "1007", "message": "The requested resource does not exist.", "code": "resource_not_exist"}]}`)
	ProjectResourceData     = []byte(`{"name": "default", "type": "PROJECT", "uuid": "a1e03f0e-92b6-4b7f-a8ec-59a45698cb82", "namespace": "testorg001", "region_id": null, "space_uuid": "", "namespace_uuid": "", "project_uuid": "", "created_by": "testorg001", "created_at": "2018-07-31T02:48:55.058Z", "teams": [], "members": []}`)
)

func setJakiroEnv(endpoint string) {
	config.GlobalConfig.Jakiro.Endpoint = endpoint
	config.GlobalConfig.Jakiro.ApiVersion = "v1"
	config.GlobalConfig.Jakiro.Timeout = 3

}

func getServer(t *testing.T, url string, data []byte, code int) *httptest.Server {
	// Start a local HTTP server
	return httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		// Test request parameters
		equals(t, req.URL.String(), url)
		// Send response to be tested
		rw.WriteHeader(code)
		rw.Write(data)

	}))
}

func TestGetResourceFromJakiro(t *testing.T) {
	server := getServer(t, "/v1/inner/resources/a1e03f0e-92b6-4b7f-a8ec-59a45698cb82", ProjectResourceData, 200)
	defer server.Close()
	setJakiroEnv(server.URL)
	resource, err := infra.GetResourceFromJakiro("a1e03f0e-92b6-4b7f-a8ec-59a45698cb82")
	ok(t, err)
	equals(t, resource.Name, "default")
}

func TestGetResourceFromJakiroByName(t *testing.T) {

	data := ProjectResourceData
	server := getServer(t, "/v1/inner/resources/alauda/PROJECT/default", data, 200)
	// Close the server when test finishes
	defer server.Close()

	setJakiroEnv(server.URL)

	resource, err := infra.GetResourceFromJakiroByName("alauda", "PROJECT", "default")
	ok(t, err)
	equals(t, resource.UUID, "a1e03f0e-92b6-4b7f-a8ec-59a45698cb82")
}

func TestGetResourceFromJakiroByNameNotFound(t *testing.T) {
	server := getServer(t, "/v1/inner/resources/alauda/PROJECT/not-exist", JakiroNotFoundErrorResp, 404)
	defer server.Close()
	setJakiroEnv(server.URL)
	resp, err := infra.GetResourceFromJakiroByName("alauda", "PROJECT", "not-exist")
	if errors.Cause(err) != common.ErrResourceNotExist {
		t.Errorf("404 check errror: %s %v", resp, errors.Cause(err))
	}
}

func TestDeleteJakiroResource(t *testing.T) {
	server := getServer(t, "/v1/inner/resources/a1e03f0e-92b6-4b7f-a8ec-59a45698cb82", nil, 204)
	defer server.Close()
	setJakiroEnv(server.URL)
	err := infra.DeleteResourceFromJakiro("a1e03f0e-92b6-4b7f-a8ec-59a45698cb82")
	ok(t, err)
}

func TestCreateJakiroResource(t *testing.T) {
	server := getServer(t, "/v1/inner/resources/", nil, 201)
	defer server.Close()
	setJakiroEnv(server.URL)

	var r model.AlaudaInternalResource
	json.Unmarshal(ProjectResourceData, &r)
	err := infra.CreateJakiroResource(&r)
	ok(t, err)
}

func TestNameCacheKey(t *testing.T) {
	key := "k8s:name:474282bd-1085-4f72-9936-b3c1eb64af15::clusterrolebindings:system:controller:persistent-volume-binder"
	name := "system:controller:persistent-volume-binder"
	query := infra.ParseNameKeyToQuery(key)
	if query.Name != name {
		t.Errorf("Error parse resource name: %s", query.Name)
	}
}

func TestGenNameCacheKey(t *testing.T) {
	target := "k8s:name:fdfcc101-5361-4adf-bf3e-919cdababeb3:kube-system:deployments:kube-dns"
	result := infra.GetKubernetesNameCacheKey("fdfcc101-5361-4adf-bf3e-919cdababeb3", "kube-dns",
		"deployments", "kube-system")
	if result != target {
		t.Errorf("Error gen name key: %s", result)
	}

	target = "k8s:name:fdfcc101-5361-4adf-bf3e-919cdababeb3::clusterroles:system:controller:deployment-controller"
	result = infra.GetKubernetesNameCacheKey("fdfcc101-5361-4adf-bf3e-919cdababeb3",
		"system:controller:deployment-controller", "clusterroles", "")
	if target != result {
		t.Errorf("Error gen name key: %s", result)
	}
}
